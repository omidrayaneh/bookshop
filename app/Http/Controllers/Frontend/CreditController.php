<?php

namespace App\Http\Controllers\Frontend;

use App\Book;
use App\Category;
use App\Comment;
use App\Order;
use App\Order_Item;
use App\Payment;
use App\Subscription;
use App\Vip;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreditController extends Controller
{
    public function index()
    {
        $categories =Category::all();
        $count= count($categories);
        $categories1 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset(0)
            ->limit($count/2)->get();
        $categories2 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset($count/2)
            ->limit($count/2)->get();

        return view('frontend.credits.index',compact(['categories1','categories2']));
    }

    public function verify(Request $request)
    {
        if (!$request->id){
            return back();
        }
        $id= $request->id;

        $sub= Subscription::whereId($id)->first();


        $order=new Order();
        $order->amount=$sub->price;
        $order->user_id=Auth::user()->id;
        $order->status=0;
        $order->save();
        $orderId=$order->id;

        $newOrder_Item=new Order_Item();
        $newOrder_Item->price=$sub->price;
        $newOrder_Item->qty=1;
        $newOrder_Item->discount_percent=0;
        $newOrder_Item->order_id=$orderId;
        $newOrder_Item->subscription_id=$sub->id;
        $newOrder_Item->save();


        $payment=new Payment($order->amount,$order->id);
        $result=$payment->doPayment();

        if ($result->Status == 100) {
            return redirect()->to('https://sandbox.zarinpal.com/pg/StartPay/'.$result->Authority);
        } else {
            echo'ERR: '.$result->Status;
            return false;
        }

    }
}
