@extends('admin.layouts.master')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>ویرایش دسته بندی </small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right"> ویرایش دسته بندی {{$category->title}}</h3>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form method="post" action="/admin/categories/{{$category->id}}">
                                @csrf
                                <input type="hidden" name="_method" value="PATCH">
                                <div class="form-group">
                                    <label for="title">نام</label>
                                    <input type="text" name="title" class="form-control" value="{{$category->title}}"
                                           placeholder="عنوان دسته بندی ...">
                                </div>
                               {{-- <div class="form-group">
                                    <label for="slug">نام مستعار</label>
                                    <input type="text" name="slug" class="form-control" value="{{$category->slug}}" placeholder="نام مستعار محصول ...">
                                </div>--}}
                                <div class="form-group">
                                    <label for="category_parent">دسته اصلی</label>
                                    <select name="category_parent" id="" class="form-control">
                                        <option value="">انتخاب کنید</option>
                                        @foreach($categories as $category_date)
                                            <option value="{{$category_date->id}}"
                                                    @if($category->parent_id==$category_date->id) selected @endIf >{{$category_date->title}}</option>
                                            @if(count($category_date->childrenRecursive)>0)
                                                @include('admin.partials.category',['categories'=>$category_date->childrenRecursive,'level'=>1,'selected_category'=>$category])
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>وضعیت انتشار در اول سایت</label>
                                    <div>
                                        <input type="radio" name="first_page" value="0" @if($category->first_page == 0) checked @endif> <span class="margin-l-10">منتشر نشده</span>
                                        <input type="radio" name="first_page" value="1" @if($category->first_page == 1) checked @endif> <span>منتشر شده</span>
                                    </div>
                                </div>
                               {{-- <div class="form-group">
                                    <label for="meta_title">عنوان ...</label>
                                    <input type="text" name="meta_title" class="form-control"
                                           value="{{$category->meta_title}}" placeholder="عنوان دسته بندی ...">
                                </div>
                                <div class="form-group">
                                    <label for="meta_desc">توضیحات ...</label>
                                    <textarea type="text" name="meta_desc" class="form-control"
                                              placeholder="توضیحات دسته بندی ...">{{$category->meta_desc}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="meta_keywords">کلمات کلیدی ...</label>
                                    <input type="text" name="meta_keywords" class="form-control"
                                           value="{{$category->meta_keywords}}" placeholder="کلمات کلیدی دسته بندی ...">
                                </div>--}}
                                <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection