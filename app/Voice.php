<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Voice extends Model
{
    protected $uploads = '/storage/voices/';
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function getPathAttribute($voice)
    {
        return $this->uploads.$voice;
    }
    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
