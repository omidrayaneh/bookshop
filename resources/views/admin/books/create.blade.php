@extends('admin.layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{asset('/admins/dist/css/dropzone.css')}}">
@endsection

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>کتاب ها</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
                <li class="active">داشبرد</li>
            </ol>
        </section>
        <section class="content">
            <section class="content">
             <div class="box box-info">
                 <div class="box-header with-border">
                     <h3 class="box-title pull-right">افزودن کتاب جدید</h3>
                 </div>
                 <!-- /.box-header -->
                 @if ($errors->any())
                     <div class="alert alert-danger">
                         <ul>
                             @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                             @endforeach
                         </ul>
                     </div>
                 @endif
                 <div class="row">
                     <div class="col-md-6 col-md-offset-3">
                         <form id="myForm" method="post" action="/admin/books" >
                             @csrf
                             <div class="form-group">
                                 <label for="title">نام کتاب</label>
                                 <input type="text" name="title" class="form-control" placeholder="نام کتاب ...">
                             </div>
                            {{-- <div class="form-group">
                                 <label for="slug">نام مستعار کتاب</label>
                                 <input type="text" name="slug" class="form-control" placeholder="نام مستعار کتاب ...">
                             </div>--}}
                             <div class="form-group">
                                 <label for="category_id">دسته بندی</label>
                                 <select name="category_id" id="" class="form-control">
                                     @foreach($categories as $category)
                                         <option value="{{$category->id}}">{{$category->title}}</option>
                                         @if(count($category->childrenRecursive)>0)
                                             @include('admin.partials.category',['categories'=>$category->childrenRecursive,'level'=>1])
                                         @endif
                                     @endforeach
                                 </select>
                             </div>
                             <div class="form-group">
                                 <label for="author_id">نویسنده ها</label>
                                 <select name="author_id" id="" class="form-control">
                                     @foreach($authors as $author)
                                         <option value="{{$author->id}}"  >{{$author->name}}</option>
                                     @endforeach
                                 </select>
                             </div>
                             <div class="form-group">
                                 <label for="narrator_id">گوینده ها</label>
                                 <select name="narrator_id" id="" class="form-control">
                                     @foreach($narrators as $narrator)
                                         <option value="{{$narrator->id}}"  >{{$narrator->name}}</option>
                                     @endforeach
                                 </select>
                             </div>
                             <div class="form-group">
                                 <label for="publisher_id">ناشر ها</label>
                                 <select name="publisher_id" id="" class="form-control">
                                     @foreach($publishers as $publisher)
                                         <option value="{{$publisher->id}}"  >{{$publisher->name}}</option>
                                     @endforeach
                                 </select>
                             </div>
                             <div class="form-group">
                                 <label>وضعیت</label>
                                 <div>
                                     <input type="radio" name="free" value="0" checked> <span class="margin-l-10">غیر رایگان</span>
                                     <input type="radio" name="free" value="1"> <span>رایگان</span>
                                 </div>
                             </div>
                             {{--<div class="form-group">
                                 <label>وضعیت برای کاربر سایت</label>
                                 <div>
                                     <input type="radio" name="free4user" value="0" checked> <span class="margin-l-10">غیر رایگان</span>
                                     <input type="radio" name="free4user" value="1"> <span>رایگان</span>
                                 </div>
                             </div>--}}
                             <div class="form-group">
                                 <label>وضعیت نشر</label>
                                 <div>
                                     <input type="radio" name="status" value="0" checked> <span class="margin-l-10">منتشر نشده</span>
                                     <input type="radio" name="status" value="1"> <span>منتشر شده</span>
                                 </div>
                             </div>
                            {{-- <div class="form-group">
                                 <label>اختصاصی</label>
                                 <div>
                                     <input type="radio" name="vip" value="0" checked> <span class="margin-l-10">عمومی</span>
                                     <input type="radio" name="vip" value="1"> <span>مشترکین</span>
                                 </div>
                             </div>--}}
                             <div class="form-group">
                                 <label>قیمت کتاب</label>
                                 <input type="number" name="price" class="form-control" placeholder="قیمت کتاب...">
                             </div>
                             <div class="form-group">
                                 <label>درصد تخفیف</label>
                                 <input type="number" name="discount_percent" class="form-control" placeholder="درصد تخفیف کتاب...">
                             </div>
                           {{--  <div class="form-group">
                                 <label>توضیحات کامل کتاب</label>
                                 <textarea id="description" type="text" name="description" class="ckeditor form-control" placeholder="توضیحات کامل کتاب..."></textarea>
                             </div>--}}
                             <div class="form-group">
                                 <label for="photo">گالری تصاویر</label>
                                 <input type="hidden" name="photo_id[]" id="book-photo">
                                 <div id="photo" class="dropzone"></div>

                             </div>
                             <div class="form-group">
                                 <label for="voice">گالری صوت</label>
                                 <input type="hidden" name="voice_id[]" id="book-voice">
                                 <div id="voice" class="dropzone"></div>
                             </div>
                             {{--<div class="form-group">
                                 <label>عنوان ...</label>
                                 <input type="text" name="meta_title" class="form-control" placeholder="عنوان کتاب ...">
                             </div>
                             <div class="form-group">
                                 <label>توضیحات ...</label>
                                 <textarea  id="tetxareaDescription" type="text" name="meta_desc" class="ckeditor form-control" placeholder="توضیحات کتاب ..."></textarea>
                             </div>
                             <div class="form-group">
                                 <label>کلمات کلیدی ...</label>
                                 <input type="text" name="meta_keywords" class="form-control" placeholder="کلمات کلیدی کتاب ...">
                             </div>--}}
                             <button type="submit" onclick="bookGallery(); voiceGallery();" class="btn btn-success pull-left">ذخیره</button>
                         </form>
                     </div>

                 </div>
             </div>
         </section>
        </section>

    </div>

@endsection
@section('script-vuejs')
    <script src="{{asset('admins/js/app.js')}}"></script>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('/admins/dist/js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{asset('/admins/plugins/ckeditor/ckeditor.js')}}"></script>
    <script>
        Dropzone.autoDiscover=false;
        var photosGallery = []
        var voicesGallery=[]
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            maxFiles: 1,
            acceptedFiles: ".jpg",
            maxFilesize: 1,
            dictDefaultMessage:"افزودن عکس کتاب",
            dictFileTooBig:"اندازه عکس بیشتر از حجم پیش فرض می باشد",// image size error message
            dictInvalidFileType:"فایل انتخابی برای این قسمت مناسب نمی باشد",// file type error message
            dictCancelUpload:" لغو آپلود عکس",//cancel error message
            dictCancelUploadConfirmation:"آیا می خواهید آپلود را متوقف کنید؟", //cancel conform
            dictRemoveFile:"حذف عکس",// remove file
            dictMaxFilesExceeded:"برای هر کتاب فقط یک عکس می توانید آپلود کنید",
            url: "{{ route('photos.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                photosGallery.push(response.photo_id)

            },

        });
        bookGallery =function () {
            document.getElementById('book-photo').value = photosGallery
        }
        var drop1 = new Dropzone('#voice', {
            addRemoveLinks: true,
            acceptedFiles: ".mp3",
            maxFiles: 20,
            maxFilesize: 256,
            paramName: "file",
            acceptedMimeTypes: null,
            acceptParameter: null,
            enqueueForUpload: true,
            dictDefaultMessage:"افزودن صوت کتاب",
            dictFileTooBig:"اندازه صوت بیشتر از حجم پیش فرض می باشد",// image size error message
            dictInvalidFileType:"فایل انتخابی برای این قسمت مناسب نمی باشد",// file type error message
            dictCancelUpload:" لغو آپلود صوت",//cancel error message
            dictCancelUploadConfirmation:"آیا می خواهید آپلود را متوقف کنید؟", //cancel conform
            dictMaxFilesExceeded:"برای هر کتاب فقط 20 صوت می توانید آپلود کنید",
            dictRemoveFile:"حذف صوت",// remove file
            url: "{{ route('voices.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                voicesGallery.push(response.voice_id)
            }
        });

        voiceGallery =function () {
            document.getElementById('book-voice').value = voicesGallery
        }
        CKEDITOR.replace('description',{
            customConfig:'config.js',
            toolbar:'simple',
            language: 'fa',
            removePlugins: 'cloudservices, easyimage'
        })
    </script>

@endsection


