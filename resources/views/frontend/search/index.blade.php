@extends('layouts.master')
@section('title')
  جستجوی کتاب
@endsection
@section('content')
    <header class="head-gradiant " >
        <div class="container">
            <h1 class="mt-5"></h1>
            <p class="tagline"></p>
            <div class="py-5">
            </div>
        </div>
    </header>
    <div class="container-fluid light-bg mb-2 text-center">
        <div class="row p-3 mt-5">
            <div class="col-12 text-center">
                <p></p>
            </div>

        </div>
    </div>

    <div class="container-fluid light-bg mb-2 text-center">
        <div class="row p-3 mt-5">
            <div class="col-12 text-center">
                <p></p>
            </div>

        </div>
    </div>
    <div class="explore pt-4 pr-3"><p>فروشگاه کتاب/داستان و رمان</p></div>
    <hr>
    <section>
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-10">
                    <div class="d-none d-md-block light-bg p-3 text-justify">
                        <h3>جستجوی کتاب</h3>
                        <p></p>
                    </div>
                    <div  class="d-lg-none">
                        <a href="#open-search" class="inline" style="text-decoration: none"><button class="btn-secondary btn-block category d-flex justify-content-between align-items-center"><span class="ti-search ti-2x"></span><h3 class="mb-0">جستجوی پیشرفته</h3><span class="ti-check ti-2x"></span></button></a>
                    </div>
                    <hr>

                    <hr>
                    <div class="books d-flex justify-content-between flex-wrap">
                        @foreach($books as $book)
                            <div class="card">
                                <a href="{{route('book',['id'=>$book->id])}}"><img src="{{$book->photos[0]->path}}" alt=""></a>
                                <div class="card-footer text-center">
                                    <h4 class="m-1">{{$book->title}}</h4>
                                    <p class="m-0"><small>{{$book->authors->name}}</small></p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

