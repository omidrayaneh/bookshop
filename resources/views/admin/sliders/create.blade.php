@extends('admin.layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{asset('/admins/dist/css/dropzone.css')}}">
@endsection

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>کتاب ها</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
                <li class="active">داشبرد</li>
            </ol>
        </section>
        <section class="content">
            <section class="content">
                <div class="box box-info">
                    <div class=" text-left">
                        <a class="btn btn-app" href="{{route('sliders.index')}}">
                            <i class="fa fa-home"></i> اسلاید ها
                        </a>
                    </div>
                    <!-- /.box-header -->
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <label for="photo">گالری تصاویر</label>
                                <input type="hidden" name="photo_id[]" id="slider-photo">
                                <div id="photo" class="dropzone"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>

    </div>

@endsection
{{--@section('script-vuejs')
    <script src="{{asset('admins/js/app.js')}}"></script>
@endsection--}}
@section('scripts')
    <script type="text/javascript" src="{{asset('/admins/dist/js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{asset('/admins/plugins/ckeditor/ckeditor.js')}}"></script>
    <script>
        Dropzone.autoDiscover=false;
        var photosGallery = []
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            maxFiles: 10,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            maxFilesize: 10,
            url: "{{ route('photos.slideUpload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                photosGallery.push(response.photo_id)
            },

        });
        bookGallery =function () {
            document.getElementById('sliders-photo').value = photosGallery
        }


    </script>

@endsection


