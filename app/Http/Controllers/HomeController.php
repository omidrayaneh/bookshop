<?php

namespace App\Http\Controllers;

use App\Book;
use App\CartModel;
use App\Category;
use App\Comment;
use App\Order;
use App\Setting;
use App\Slider;
use App\Subscription;
use App\User;
use App\Voice;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use function Sodium\add;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*  public function __construct()
      {
          $this->middleware('auth');
      }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $carts = CartModel::all();


        if ($carts) {
            Cart::destroy();
            foreach ($carts as $cart) {
                $book=Book::whereId($cart->id)->first();
                if (!$book)
                {
                    $b=  DB::table('cart_models')->where('id',$cart->id)->delete();
                }
                $discont=($book->price*$book->discount_percent)/100;
                if ($cart->discount!=$discont || $book->status==0 || $book->free != $cart->free){
                    $b=  DB::table('cart_models')->where('id',$cart->id)->delete();
                }
                elseif ($cart->free==0 &&  $book->price !=$cart->price ){
                    $b=  DB::table('cart_models')->where('id',$cart->id)->delete();
                }
                    $added = Cart::add([
                        'id' => $cart->id,
                        'name' => $cart->name,
                        'qty' => '1',
                        'price' => $cart->price,
                        'discount' => $cart->discount,
                        'weight' => '',
                        'discountRate' => $cart->discount,
                        'options' => [
                            'path' => $cart->path,
                            'discount' => $cart->discount,
                            'free' => $cart->free
                        ],
                    ]);



            }
        }

        $categories= Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
         $count = count($categories);
         $categories1 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset(0)
            ->limit($count / 2)->get();
        $categories2 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset($count / 2)
            ->limit($count / 2)->get();

        $books = Book::with('authors', 'photos')
            ->where('status',1)
            ->get();

        $comments = Comment::where('status', 1)->with('book')->limit(6)->get();

        $subscriptions = Subscription::all();
        $category_books = DB::table('books')
            ->leftJoin('categories', 'books.category_id', '=', 'categories.id')
            ->leftJoin('book_photo', 'book_photo.book_id', '=', 'books.id')
            ->leftJoin('photos', 'photos.id', '=', 'book_photo.photo_id')
            ->leftJoin('authors', 'authors.id', '=', 'books.author_id')
            ->select( 'categories.*', 'photos.*','authors.*','books.*')
            ->where('categories.first_page',1)
            ->where('books.status',1)
            ->get(); //most be book is lasted SELECT in join
        $visited_books = DB::table('books')
            ->leftJoin('categories', 'books.category_id', '=', 'categories.id')
            ->leftJoin('book_photo', 'book_photo.book_id', '=', 'books.id')
            ->leftJoin('photos', 'photos.id', '=', 'book_photo.photo_id')
            ->leftJoin('authors', 'authors.id', '=', 'books.author_id')
            ->select( 'books.*','categories.*', 'photos.*','authors.*','books.*')
            ->orderByDesc('visit')
            ->where('books.status',1)
            ->limit(20)->get();; //most be book is lasted SELECT in join
        $lasted_add_books = DB::table('books')
            ->leftJoin('categories', 'books.category_id', '=', 'categories.id')
            ->leftJoin('book_photo', 'book_photo.book_id', '=', 'books.id')
            ->leftJoin('photos', 'photos.id', '=', 'book_photo.photo_id')
            ->leftJoin('authors', 'authors.id', '=', 'books.author_id')
            ->select( 'books.*','categories.*', 'photos.*','authors.*','books.*')
            ->orderByDesc('books.created_at')
            ->where('books.status',1)
            ->limit(20)->get();; //most be book is lasted SELECT in join
        $ordered_books = DB::table('books')
            ->leftJoin('categories', 'books.category_id', '=', 'categories.id')
            ->leftJoin('order__items', 'order__items.book_id', '=', 'books.id')
            ->leftJoin('orders', 'orders.id', '=','order__items.order_id' )
            ->leftJoin('book_photo', 'book_photo.book_id', '=', 'books.id')
            ->leftJoin('photos', 'photos.id', '=', 'book_photo.photo_id')
            ->leftJoin('authors', 'authors.id', '=', 'books.author_id')
            ->select( 'books.*','categories.*', 'photos.*','authors.*','books.*')
            ->where('orders.status',1)
            ->where('books.status',1)
            ->limit(20)->get(); //most be book is lasted SELECT in join
        $all_category=Category::where('first_page',1)->get();
        $slider=Slider::all();
        $first_slide=Slider::first();
        $setting=Setting::first();
        return view('frontend.home.index', compact(['categories1', 'categories2', 'books', 'comments',
            'subscriptions','category_book','category_books','all_category','first_slide',
            'slider','visited_books','lasted_add_books','ordered_books','setting']));

    }



    public function admin()
    {
        $orders = Order::where('status', 1)->get();
        $books = Book::where('status', 1)->get();
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->paginate(10);

        return view('admin.dashboard', compact(['orders', 'books', 'categories']));
    }






}
