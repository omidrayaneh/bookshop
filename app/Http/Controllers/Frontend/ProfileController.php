<?php

namespace App\Http\Controllers\Frontend;

use App\Author;
use App\Category;
use App\Order;
use App\Payment;
use App\Subscription;
use App\User;
use App\Vip;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Hekmatinasser\Verta\Verta;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $book_purchased = DB::table('orders')
            ->Join('order__items', 'orders.id', '=', 'order__items.order_id')
            ->Join('books', 'order__items.book_id', '=', 'books.id')
            ->Join('book_photo', 'book_photo.book_id', '=', 'books.id')
            ->Join('photos', 'book_photo.photo_id', '=', 'photos.id')
            ->select( 'order__items.*', 'orders.*','photos.*','books.*')
            ->where('orders.user_id',Auth::user()->id)
            ->where('orders.status',1)
            ->paginate(10);

        $orders = DB::table('orders')
            ->leftJoin('payments', 'orders.id', '=', 'payments.order_id')
            ->select( 'payments.*', 'orders.*')
            ->where('orders.user_id',Auth::user()->id)->orderBy('orders.created_at','DESC')
            ->paginate(10);
        $categories =Category::all();
        $count= count($categories);
        $categories1 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset(0)
            ->limit($count/2)->get();
        $categories2 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset($count/2)
            ->limit($count/2)->get();


        $vip=Vip::where([['user_id',Auth::user()->id],['status','>', 0]])->whereColumn('start_day','<', 'end_day')->first();
        $message='';
        if ($vip){
            $date =Verta($vip->end_day);
            $book =$vip->status;
            $message= '   تا  تاریخ  ' .$date .'  تعداد کتاب باقی مانده  '. $book;
        }
        else{
            $message=0;
        }
        Session::flash('credit','  مانده اعتبار شما   '.$message);

        return view('frontend.profile.index',compact(['categories1','categories2','books','comments',
            'subscriptions','vip','orders','book_purchased']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function rename(Request $request)
    {
        if ($request->newpass==null || $request->renewpass==null){
            $user =User::findOrFail(Auth::user()->id);
            $user->name=$request->name;
            $user->last_name=$request->last_name;
            $user->save();
            Session::flash('success', 'تغییرات شما اعمال شد');

            return redirect('/profile');
        }
        elseif(isset($request->newpass) && isset($request->renewpass) ){
            if (strlen ($request->newpass)<8){
                Session::flash('error', 'رمز عبور کمتر از 8 کاراکتر است');
                return redirect('/profile');
            }
            elseif (strlen ($request->newpass)<8){
                Session::flash('error', 'رمز عبور کمتر از 8 کاراکتر است');
                return redirect('/profile');
            }elseif($request->newpass!=$request->renewpass){
                Session::flash('error', 'تکرار رمز اشتباه است، دوباره سعی کنید');
                return redirect('/profile');
            }
            else{
                $user =User::findOrFail(Auth::user()->id);
                $user->name=$request->name;
                $user->last_name=$request->last_name;
                $user->password=Hash::make($request->newpass);
                $user->save();
                Session::flash('success', 'تغییرات شما اعمال شد');
                Auth::logout();
                Session::flush();
                return redirect('/');            }

        }

    }
}
