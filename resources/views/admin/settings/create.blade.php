@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>تنظیم صفه اول</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right"> نمایش گروه ها در صفحه اول سایت</h3>
                </div>
                @if(Session::has('setting-message'))
                    <div class="alert alert-danger">
                        <div class="text-center">{{session('setting-message')}}</div>
                    </div>
            @endif
            <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form method="post" action="/admin/settings">
                                @csrf
                                <div class="form-group">
                                    <label>نمایش جدید ترین کتاب ها</label>
                                    <div>
                                        <input type="radio" name="last" value="1" checked> <span class="margin-l-10">نشان داده شود</span>
                                        <input type="radio" name="last" value="0"> <span>نشان داده نشود</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>نمایش پربازدیدترین کتاب ها</label>
                                    <div>
                                        <input type="radio" name="visit" value="1" checked> <span class="margin-l-10">نشان داده شود</span>
                                        <input type="radio" name="visit" value="0"> <span>نشان داده نشود</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>نمایش پرفروش ترین کتاب ها</label>
                                    <div>
                                        <input type="radio" name="order" value="1" checked> <span class="margin-l-10">نشان داده شود</span>
                                        <input type="radio" name="order" value="0"> <span>نشان داده نشود</span>
                                    </div>
                                </div>
                                @if($settings->count()==0)
                                    <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                                @else
                                    <button type="submit" disabled class="btn btn-success pull-left">ذخیره</button>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection