@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>کاربر ها</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right">ایجاد کاربر جدید</h3>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form method="post" action="/admin/users">
                                @csrf
                                <div class="form-group">
                                    <label for="name">نام</label>
                                    <input type="text" name="name" class="form-control" placeholder="نام کاربر ...">
                                </div>
                                <div class="form-group">
                                    <label for="last_name">نام خانوادگی</label>
                                    <input type="text" name="last_name" class="form-control" placeholder="نام خانوادگی کاربر ...">
                                </div>
                                <div class="form-group">
                                    <label for="email">ایمیل</label>
                                    <input type="email" name="email" class="form-control" placeholder="نام خانوادگی کاربر ...">
                                </div>
                                <div class="form-group">
                                    <label for="password">رمز عبور</label>
                                    <input type="password" name="password" class="form-control" placeholder="رمز عبور را وارد کنید ...">
                                </div>
                                <div class="form-group">
                                    <label for="re_password">تکرار رمز عبور</label>
                                    <input type="password" name="re_password" class="form-control" placeholder="تکرار رمز عبور را وارد کنید ...">
                                </div>
                                <div class="form-group">
                                    <label>وضعیت کاربری</label>
                                    <div>
                                        <input type="radio" name="role_status" value="0" checked> <span class="margin-l-10">کاربر عادی</span>
                                        <input type="radio" name="role_status" value="1"> <span>کاربر سایت</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>وضعیت</label>
                                    <div>
                                        <input type="radio" name="status" value="0" checked> <span class="margin-l-10">فعال</span>
                                        <input type="radio" name="status" value="1"> <span>غیر فعال</span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection