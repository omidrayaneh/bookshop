<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings=Setting::all();
        return view('admin.settings.index',compact(['settings']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $settings=Setting::all();
        if ($settings->count()>=1){
            Session::flash('setting-message', 'برای ایجاد تنظیم جدید باید تنظیم قدیم را حذف کنید');
        }

        return view('admin.settings.create',compact(['settings']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newSetting=new Setting();
        $newSetting->last=$request->input('last');
        $newSetting->visit=$request->input('visit');
        $newSetting->order=$request->input('order');
        $newSetting->save();
        Session::flash('success', 'تنظیم ها اعمال شد');
        return redirect('/admin/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting=Setting::findOrFail($id);
        return view('admin.settings.edit',compact(['setting']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $setting=Setting::findOrFail($id);
        $setting->last=$request->input('last');
        $setting->visit=$request->input('visit');
        $setting->order=$request->input('order');
        $setting->save();
        Session::flash('success', 'تنظیم ها اعمال شد');
        return redirect('/admin/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $setting=Setting::findOrFail($id);
        $setting->delete();
        Session::flash('success', 'تنظیم ها حذف شد');
        return redirect('/admin/settings');
    }
}
