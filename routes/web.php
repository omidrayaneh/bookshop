<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('frontend.home.index');
});*/

Auth::routes();

//Route::get('frontend/home/index', 'HomeController@index')->name('/');
Route::get('/', 'HomeController@index');
Route::post('register-user', 'Frontend\UserController@register')->name('user.register');

Route::post('/','Frontend\BookController@search')->name('book.search');
Route::get('category-search','Frontend\BookController@searchByCategory')->name('category.search');

Route::get('authors-search','Frontend\BookController@searchByAuthor')->name('author.search');
Route::get('narrator-search','Frontend\BookController@searchByNarrator')->name('narrator.search');
Route::get('publisher-search','Frontend\BookController@searchByPublisher')->name('publisher.search');


Route::get('book/{id}','Frontend\BookController@book')->name('book');
Route::post('add-comment/{id}','Frontend\BookController@addComment')->name('add.comment');

Route::group(['middleware'=>'auth'],function (){

    Route::get('add','Frontend\CartController@add')->name('add');
    Route::get('cart','Frontend\CartController@index');

    Route::post('/remove-item/{id}', 'Frontend\CartController@removeItem')->name('cart.remove');

    Route::get('order-verify','Frontend\OrderController@verify')->name('order.verify');
    Route::get('payment-verify/{id}','Frontend\PaymentController@verify')->name('payment.verify');
    Route::get('credit','Frontend\CreditController@index')->name('credit');
    Route::PATCH('credit-verify/{id}','Frontend\CreditController@verify')->name('credit.verify');

    Route::resource('profile','Frontend\ProfileController');
    Route::PATCH('profile-rename/{id}','Frontend\ProfileController@rename')->name('rename');




});

Route::group(['middleware' => ['admin']], function(){
    Route::prefix('admin/')->group(function (){
        Route::get('/', 'HomeController@admin')->middleware('admin')->name('admin');

        Route::resource('/categories', 'Admin\CategoryController');

        Route::resource('photos','Admin\PhotoController');
        Route::post('photos/upload','Admin\PhotoController@upload')->name('photos.upload');
        Route::post('photos/slideUpload','Admin\PhotoController@slideUpload')->name('photos.slideUpload');

        Route::resource('voices','Admin\VoiceController');
        Route::post('voices/upload','Admin\VoiceController@upload')->name('voices.upload');

        Route::resource('books','Admin\BookController');
        Route::resource('subscriptions','Admin\SubscriptionController');
        Route::resource('publishers','Admin\PublisherController');
        Route::resource('authors','Admin\AuthorController');
        Route::resource('narrators','Admin\NarratorController');
        Route::resource('banks','Admin\BankController');

        Route::get('comments','Admin\CommentController@index')->name('comments.index');
        Route::post('actions/{id}', 'Admin\CommentController@actions')->name('comments.actions');

        Route::delete('comments/{id}', 'Admin\CommentController@destroy')->name('comments.destroy');

        Route::get('orders','Admin\OrderController@index')->name('orders.index');
        Route::resource('users','Admin\UserController');
        Route::resource('sliders','Admin\SliderController');
        Route::resource('settings','Admin\SettingController');

    });

});
