<?php

namespace App\Http\Controllers\Admin;

use App\Narrator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Validator;

class NarratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $narrators=Narrator::paginate(10);
        return view('admin.narrators.index',compact('narrators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.narrators.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'required|unique:narrators',
        ],[
            'name.required'=>'عنوان گوینده  خالیست',
            'name.unique'=>'این گوینده قبلا ثبت شده است',
        ]);
        if($validator->fails()){
            return redirect('/admin/narrators/create')->withErrors($validator)->withInput();
        }
        else {
            $narrators = new Narrator();
            $narrators->name = $request->name;
            $narrators->status = $request->status;
            $narrators->save();
            Session::flash('success', 'گوینده با موفقیت اضافه شد.');
            return redirect('/admin/narrators');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $narrator =Narrator::findOrfail($id);
        return view('admin.narrators.edit', compact(['narrator']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'required|unique:narrators,name,'.$id,
        ],[
            'name.required'=>'عنوان گوینده  خالیست',
            'name.unique'=>'این گوینده قبلا ثبت شده است',
        ]);
        if($validator->fails()){
            return redirect('/admin/narrators/'.$id.'/edit')->withErrors($validator)->withInput();

        }
        else {
            $narrator = Narrator::findOrFail($id);
            $narrator->name = $request->name;
            $narrator->status = $request->status;
            $narrator->save();
            Session::flash('success', 'گوینده با موفقیت ویرایش شد.');
            return redirect('/admin/narrators');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $narrator = Narrator::findOrFail($id);
        $narrator->delete();

        Session::flash('error', 'گوینده با موفقیت حذف شد.');
        return redirect('/admin/narrators');
    }
}
