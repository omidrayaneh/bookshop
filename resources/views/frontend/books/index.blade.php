@extends('layouts.master')
@section('title')
    کتاب {{ $book->title  }}
@endsection
@section('styles')
    <link rel="stylesheet" href="/frontend/css/audio-player.css">
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
@endsection
@section('content')

    <header class="head-gradiant " >
            <div class="container">
                <h1 class="mt-5"></h1>
                <p class="tagline"></p>
                <div class="py-5">
                </div>
            </div>
    </header>
    <div class="container-fluid light-bg mb-2 text-center">
        <div class="row p-3 mt-5">
            <div class="col-12 text-center">
                <p></p>
            </div>

        </div>
    </div>

    <div class="container-fluid light-bg mb-2 text-center">
        <div class="row p-3 mt-5">
            <div class="col-12 text-center">
                <p></p>
            </div>

        </div>
    </div>
    @if ($errors->any())
        <div class="text-center alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('success'))
        <div class="text-center alert alert-success">
            <div>{{session('success')}}</div>
        </div>
    @endif
    <div class="container text-center mb-5">
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> پلیر پخش صوتی کتاب {{$book->title}}</h5>
                    </div>
                    <div class="modal-body">

                        <section class="audio-player card">
                            <div class="card">
                                <div class="card-body pb-1 pt-2">
                                    <h2 class="card-title col text-center">{{$book->title}}</h2>
                                    <div class="row align-items-center mt-4 mb-3 mx-0">
                                        <i id="play-button"class="material-icons play-pause text-primary mr-2" aria-hidden="true"><span class="ti-control-play"></span></i>
                                        <i id="pause-button"class="material-icons play-pause d-none text-primary mr-2" aria-hidden="true"><span class="ti-control-pause"></span></i>
                                        <i id="next-button"class="material-icons text-primary ml-2 mr-3" aria-hidden="true"><span class="ti-control-skip-forward"></span></i>
                                        <div class="col ml-auto rounded-circle border border-primary p-1">
                                            <img id="thumbnail" class="img-fluid rounded-circle" src="{{$book->photos[0]->path}}" alt="">
                                        </div>
                                    </div>
                                    <div class="p-0 m-0" id="now-playing">
                                        <p class="font-italic mb-0">در حال پخش: </p>
                                        <p class="lead" id="title"></p>
                                    </div>
                                    <div class="progress-bar  col-12 mb-3">
                                    </div>
                                </div>
                                <ul class="playlist list-group list-group-flush">

                                    @foreach($free_voice as $fv)
                                    @if(isset($fv->path) && $fv->free==1)
                                            <li style="display: block !important;" audio_url="{{$fv->path}}"
                                                class="list-group-item playlist-item py-1">
                                                <div class="d-flex flex-row justify-content-between align-items-center">
                                                    <div>نمونه</div>
                                                    <div><a href="{{$fv->path}}" class="btn btn-secondary">دانلود</a></div>
                                                </div>
                                            </li>
                                        @endif
                                    @endforeach
                                    @if(Auth::check())
                                        @foreach($not_free_voice as $nfv)
                                            @if(isset($nfv->path))
                                                    <li style="display: block !important;" audio_url="{{$nfv->path}}"
                                                        class=" list-group-item playlist-item py-1">
                                                        <div class="d-flex flex-row justify-content-between align-items-center">
                                                            <div>پخش</div>
                                                            <div><a href="{{$fv->path}}" class="btn btn-secondary">دانلود</a></div>
                                                        </div>
                                                    </li>
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                                <!-- <div class="card-body">
                                <a href="#" class="card-link">Card link</a>
                                <a href="#" class="card-link">Another link</a>
                              </div> -->
                            </div>
                            <audio id="audio-player" class="d-none" src="" type="audio/mp3" controls="controls"></audio>
                        </section>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-3 mb-md-0">
            <div class="book-img col-md-3">
                <img width="250" height="350" src="{{$book->photos[0]->path}}" alt="">
            </div>
            <div class="mt-md-5 col-md-6 text-md-right">
                <h3>{{$book->title}}</h3>
                <p class="mb-1">نویسنده: <a href="{{route('author.search',['author_id'=>$book->author_id])}}" >{{$book->authors->name}}</a></p>
                <p class="mb-1">گوینده: <a href="{{route('narrator.search',['narrator_id'=>$book->narrator_id])}}">{{$book->narrators->name}}</a></p>
                <p class="mb-1">ناشر: <a href="{{route('publisher.search',['publisher_id'=>$book->publisher_id])}}">{{$book->publishers->name}}</a></p>
                <p class="h5 mt-5"> نسخه الکترونیک کتاب <span>{{ $book->title }} </span> به همراه هزاران کتاب دیگر به
                    صورت کاملا قانونی در دسترس است.</p>
            </div>
            <div class="mt-5 col-md-3">
                <div class="card">
                    @php
                    $price = $book->price;
                    $discount_percent = $book->discount_percent;
                    $discount = $price * $discount_percent / 100;
                    $book_price = $price - $discount;
                    @endphp
                    <div class="card-header"><h3 class="text-center">@if($book->free==0){{$book_price  }} تومان @else
                                رایگان @endif </h3></div>
                    <div class="card-body">
                        <form method="post" action="{{url('add',$book)}}">
                            @csrf
                            <a class="btn-primary btn-lg btn-block py-4"
                               href=" @if(!Auth::check()) {{route('login')}} @else {{route('add', ['id' => $book])}} @endif ">
                               افزودن به سبد خرید</a>
                        </form>
                        <button data-toggle="modal" style="margin-top: 5px;" data-target="#exampleModal" class="btn-secondary btn-lg btn-block py-4"> شنیدن نسخه نمونه</button>
                       {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">--}}
                        {{--    پخش
                        </button>--}}
                           {{-- @foreach($free_voice as $fv)
                                @if(isset($fv->path) && $fv->free==1)
                               <audio  src="{{$fv->path}}" controls ></audio>
                                @endif
                            @endforeach--}}
                       {{-- @if(Auth::check())
                            @foreach($not_free_voice as $nfv)
                                @if(isset($nfv->path))
                                    <audio  src="{{$nfv->path}}" controls ></audio>

                                @endif
                            @endforeach
                        @endif--}}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <section>
        <div class="section pt-2 pt-md-3" id="comments" @if(!$comments->count()) style="display: none" @endif>
            <div class="container">
                <div class="card p-3">
                    <div class="section-title mb-1">
                        <h3>نظرات کاربران در مورد این کتاب:</h3>
                    </div>
                    <div class="row">
                        <div class="col-12 mb-0 mb-md-2">
                            <div class="card features">
                                <div class="card-body">
                                    <div class="media">
                                        {{--<img class="profile pl-2" src="/frontend/images/profile2.png" alt="">--}}
                                        <div class="media-body">
                                            @foreach($comments as $comment)
                                                <div class="card-title"><h4>{{$comment->title}}</h4></div>
                                                <div class="card-text">{{$comment->description}}</div>
                                                <hr>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container mt-4 mt-md-5">
            <div class="comment row w-50">
                <div class="col">
                    <h5>نظر خود را بنویسید</h5>
                    <div class="contact-form">
                        <form class="form-group" action="{{route('add.comment',['id'=>$book->id])}}" method="post">
                            @csrf
                            <p><input class="form-control" name="name" type="text" placeholder="نام و نام خانوادگی"
                                      value=""></p>
                            <p><input class="form-control" name="email" type="email" placeholder="پست الکترونیک"
                                      value="">
                            </p>
                            <p><input class="form-control" name="title" type="text" placeholder="موضوع" value=""></p>
                            <p><textarea class="form-control" name="description" placeholder="متن شما" rows="8"
                                         cols="80"></textarea></p>
                            <p><input class="form-control btn btn-secondary btn-block" type="submit" name=""
                                      value="ارسال"></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <hr>
@endsection
@section('scripts')
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha384-xBuQ/xzmlsLoJpyjoggmTEz8OWUFM0/RC5BsqQBDX2v5cMvDHcMakNTNrHIW2I5f" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/frontend/js/jquery-ui-slider.js"></script>
    <script type="text/javascript" src="/frontend/js/audioPlayer.js"></script>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36251023-1']);
        _gaq.push(['_setDomainName', 'jqueryscript.net']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
@endsection