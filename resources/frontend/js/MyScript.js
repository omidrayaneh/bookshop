var tabs = document.querySelectorAll('.col-tabs-list li a');
tabs.forEach(function(tab,index){
  tab.addEventListener('click', function(e){
    e.preventDefault();
    document.querySelector('.col-tabs-list li a.selected').classList.remove('selected');
    this.classList.add('selected');
    var databox = this.getAttribute('data-content');
    document.querySelector('.col-tabs-text li.selected').classList.remove('selected');
    document.querySelector(".col-tabs-text li[data-content = "+ databox +"]").classList.add('selected');
  })
})
