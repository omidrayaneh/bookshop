@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>تنظیمات درگاه بانکی</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right">ویرایش درگاه بانکی</h3>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form method="post" action="/admin/banks/{{$bank->id}}">
                                @csrf
                                <input type="hidden" name="_method" value="PATCH">
                                <div class="form-group">
                                    <label for="id">شناسه بانک</label>
                                    <input type="text" name="id" value="{{$bank->merchantID}}" class="form-control" placeholder="merchantID ...">
                                </div>
                                <div class="form-group">
                                    <label for="name">عنوان بانک</label>
                                    <input type="text" name="name" value="{{$bank->name}}" class="form-control" placeholder="نام بانک">
                                </div>
                                <div class="form-group">
                                    <label for="description">توضیحات</label>
                                    <input type="text" name="description" value="{{$bank->description}}" class="form-control" placeholder="description...">
                                </div>
                                <div class="form-group">
                                    <label>وضعیت درگاه</label>
                                    <div>
                                        <input type="radio" name="status" value="1" @if($bank->status == 1) checked @endif> <span>فعال</span>
                                        <input type="radio" name="status" value="0" @if($bank->status == 0) checked @endif> <span class="margin-l-10">غیر فعال</span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection