<?php

namespace App\Http\Controllers\Frontend;

use App\Book;
use App\Order;
use App\Order_Item;
use App\Payment;
use App\Vip;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    public function verify()
    {

        if (!Cart::content()){
            return redirect('/');
        }

        $bookId=[];
        $price=0;
        $free=0;
        foreach (Cart::Content() as $cart){
            $price=$cart->price;
            $free=$cart->options->free;
            $bookId[$cart->id]=$cart->qty;
        }


        $order=new Order();

        $order->amount=$price;
        $order->user_id=Auth::user()->id;
        $order->status=0;
        $order->save();
        $orderId=$order->id;

        foreach (Cart::content() as $item)//add order items
        {

            $newOrder_Item=new Order_Item();
            $newOrder_Item->price=$item->price;
            $newOrder_Item->qty=1;
            $newOrder_Item->discount_percent=$item->discount;
            $newOrder_Item->order_id=$orderId;
            $newOrder_Item->book_id=$item->id;
            $newOrder_Item->save();
        }
        if ($price==0){

            $order=Order::findOrFail($orderId);
            $order->status=1;
            $order->save();

            Session::flash('success', 'پرداخت  با موفقیت انجام شد');
            $b=  DB::table('cart_models')->delete();

            Cart::destroy();
            return redirect('/cart');
        }
        else{
            $payment=new Payment($order->amount,$order->id);
            $result=$payment->doPayment();
            if ($result->Status == 100) {
                return redirect()->to('https://sandbox.zarinpal.com/pg/StartPay/'.$result->Authority);
                //change this =>  'https://sandbox.zarinpal.com/pg/StartPay/  to this => 'Location: https://www.zarinpal.com/pg/StartPay/'
            } else {
                Session::flash('error', 'مشکلی در پرداخت بوجود آمد، با پشتیبانی تماس بگیرید');
                return redirect('/');
                //echo'ERR: '.$result->Status;
            }
        }

    }
}
