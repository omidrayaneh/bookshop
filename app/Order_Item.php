<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_Item extends Model
{
    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
