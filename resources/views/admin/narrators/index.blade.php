@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>گوینده ها</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right">لیست گوینده ها </h3>
                    <div class=" text-left">
                        <a class="btn btn-app" href="{{route('narrators.create')}}">
                            <i class="fa fa-plus"></i> جدید
                        </a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            <div>{{session('success')}}</div>
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <div>{{session('error')}}</div>
                        </div>
                    @endif
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th class="text-right">ردیف</th>
                                    <th class="text-right">شناسه</th>
                                    <th class="text-right">نام نوسنده</th>
                                    <th class="text-right">وضعیت</th>
                                    <th class="text-center">عملیات</th>

                                </tr>
                                </thead>
                                <tbody>
                                @php $i=1 @endphp
                                @foreach($narrators as $narrator)
                                    <tr>
                                        <td class="text-right">{{$i}}</td>
                                        <td class="text-right">{{$narrator->id}}</td>
                                        <td class="text-right">{{$narrator->name}}</td>
                                        @if($narrator->status==1)
                                            <td><span class="label label-success">فعال</span></td>
                                        @else
                                            <td><span class="label label-danger">غیر فعال</span></td>
                                        @endif
                                        <td class="text-center">
                                            <div class="display-inline-block">
                                                <form method="post" action="/admin/narrators/{{$narrator->id}}">
                                                    <a href="{{route('narrators.edit',$narrator->id,compact($narrator))}}" class="btn btn-warning" >ویرایش</a>
                                                    @csrf
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button type="submit"  class="btn btn-danger " >حذف</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @php $i++; @endphp
                                @endforeach
                                </tbody>
                            </table>
                            <div class="col-md-12" style="text-align: center">{{$narrators->links()}}</div>

                        </div>
                    <!-- /.table-responsive -->
                </div>
            </div>
        </section>

    </div>
@endsection