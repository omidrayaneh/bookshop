<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function books(){
        return $this->belongsToMany(Book::class)->withPivot('qty');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function payment(){
        return $this->hasOne(Payment::class);
    }
    public function order_items()
    {
        return $this->belongsTo(Order_Item::class);
    }
}
