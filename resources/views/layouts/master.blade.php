<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport' />
    <link rel="stylesheet" href="/frontend/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/frontend/css/bootstrap.css">
    <link rel="stylesheet" href="/frontend/css/bootstrap-v4-rtl.css">
    <link rel="stylesheet" href="/frontend/css/themify-icons.css">
    <link rel="stylesheet" href="/frontend/css/colorbox.css">
    <link rel="stylesheet" href="/frontend/style.css">
    @yield('styles')

    <title>@yield('title')</title>
    <meta name="keywords" content="کتاب،مجله،روزنامه،مقاله،خرید،رایگان،آسان،رایگان،خرید راحت،تاریخی،رمان،شهر،داستان">
    <meta name="description" content="فروشگاه کتاب،کتاب صوتی،کتاب تاریخی،جواد والی زاده،گویندگان برتر،شنیدنی،روانشناسی،خرید آسان" />
</head>
<body data-spy="scroll" data-target="#navbar" data-offset="30">


<div style="display:none">
    <div class="categories d-flex flex-row" id="open-category">
        <div class="col-6">
            <form  method="post" action="{{route('category.search')}}">
                @csrf
            @foreach($categories1 as $category)
                <h4><a  href="{{route('category.search',['id'=>$category->id])}}">{{$category->title}}</a></h4>
            @endforeach
            </form>
        </div>
        <div class="col-6">
        <form  method="post" action="{{route('category.search')}}">
            @csrf
            @foreach($categories2 as $category)
                <h4><a  href="{{route('category.search',['id'=>$category->id])}}">{{$category->title}}</a></h4>
            @endforeach
        </form>
        </div>
    </div>
</div>
<div class="nav-menu fixed-top">
    <div class="container">
        <div class="row" style="margin-top:12px;">
            <div class="col">
                <div class="navbar navbar-dark navbar-expand-lg">

                    {{--<a href="{{url('/')}}" class="navbar-brand">فروشگاه کتاب</a>--}}
                    <a href="{{url('/')}}"><img style="width:270px;" src="/frontend/images/logo.png"></a>
                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbar">
                        <ul class="navbar-nav mr-auto">
                            {{--<li class="nav-item"><a href="#" class="nav-link"><span class="ti-world ti-1x ml-2"></span>بلاگ</a>
                            </li>
                            <li class="nav-item"><a href="#" class="nav-link"><span class="ti-gift ti-1x ml-2"></span>هدیه</a>
                            </li>--}}

                            <li class="nav-item"   @if(!Auth::check()) style="display: none" @endif><a href="{{route('profile.index')}}" class="nav-link"><span class="ti-user ti-1x ml-2"></span>حساب کاربری</a>
                            </li>
                            <li class="nav-item" @if(!Auth::check()) style="display: none;" @endif ><a data-toggle="dropdown" data-loading-text="بارگذاری ..."
                                                    class="nav-link heading dropdown-toggle">
                                    <span class="cart-icon pull-left flip"></span>
                                    <span class="ti-shopping-cart ti-1x ml-2"></span>سبد خرید</a>
                                @if(Cart::content()->count())
                                <ul class="dropdown-menu">
                                        <li>
                                            <table class="table">
                                                <tbody>
                                                @foreach(Cart::content() as $row)
                                                    <tr>
                                                        <td class="text-center"><a href="#"><img class="img-thumbnail"
                                                                                                 title="{{$row->path}}"
                                                                                                 width="40" height="40"
                                                                                                 src="{{$row->options->path}}"></a>
                                                        </td>
                                                        <td class="text-left"><a href="#">{{$row->name}}</a></td>
                                                        <td class="text-right">{{$row->price}} تومان</td>
                                                        <td class="text-center">
                                                            <a class="text-danger" title="حذف" onclick="event.preventDefault();
                                                                    document.getElementById('remove-cart-item_{{$row->rowId}}').submit();"
                                                                    >&#10006;</a>
                                                        </td>
                                                        <form id="remove-cart-item_{{$row->rowId}}" action="{{ route('cart.remove', ['id' => $row->rowId]) }}"
                                                              method="post" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </li>
                                        <li>
                                            <div>
                                                <table class="table table-bordered">
                                                    <tbody>
                                                    <tr>
                                                        <?php $total_price=0;
                                                        $total_discount=0;
                                                        $total=0;
                                                        ?>
                                                        @foreach(Cart::content() as $row)
                                                            <?php
                                                                $total_price += $row->price+$row->options->discount;
                                                                $total_discount += $row->options->discount;
                                                                $total+=$row->price;
                                                                ?>
                                                        @endforeach
                                                        <td class="text-right"><strong>جمع کل</strong></td>
                                                        <td class="text-right">{{$total_price}} تومان</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right"><strong>تخفیف</strong></td>
                                                        <td class="text-right">{{$total_discount}} تومان</td>

                                                    </tr>
                                                    <tr>
                                                        <td class="text-right"><strong>قابل پرداخت</strong></td>
                                                        <td class="text-right">{{$total}} تومان</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <p class="checkout">
                                                    <a href="{{url('cart')}}" class="btn btn-primary"><i
                                                                class="fa fa-shopping-cart"></i> مشاهده سبد</a>&nbsp;&nbsp;&nbsp;
                                                    <a href="{{ route('order.verify') }}" class="btn btn-primary"><i class="fa fa-share"></i> تسویه حساب</a>
                                                </p>
                                            </div>
                                        </li>
                                </ul>
                            @else
                                    <ul class="dropdown-menu " >
                                        <li class="text-center" style="color: #ff760c;">
                                            <span class="text-center">سبد خرید شما خالی است</span>
                                        </li>
                                    </ul>
                            @endif

                            @if(!Auth::check())
                                <li class="nav-item"><a href="{{route('login')}}"
                                                        class="btn btn-outline-light ">ورود</a></li>
                            @else
                                <li class="nav-item"><a href="{{route('logout')}}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();"
                                                        class="btn btn-outline-light ">خروج</a>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="post"
                                      style="display: none;">
                                    @csrf
                                </form>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex flex-lg-row flex-column">
            <div class="col-12 col-lg-4">
                <div>
                    <a href="#open-category" class="inline" style="text-decoration: none">
                        <button class="mt-md-2 btn-secondary btn-block category d-flex justify-content-between align-items-center">
                            <span class="ti-angle-left ti-2x"></span>
                            <h3 class="mb-0">دسته بندی ها</h3><span class="ti-view-list-alt ti-2x"></span></button>
                    </a>
                </div>
            </div>
            <div class="col-12 col-lg-8">
                <form class="card form-group  mt-2" method="POST" action="{{route('book.search')}}">
                    @csrf
                    <input type="hidden" name="search" value="">
                    <div class="input-group">
                        <input type="text" name="search"  class="form-group-item form-control"
                               placeholder="جستجو در بین کتب، مقالات، مجلات، رمان ها و ...">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary"><span class="ti-search ti-1x"></span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@yield('content')
<footer>
    <p class="mb-1">کلیه حقوق محصولات و محتوای این سایت متعلق به Masneed می باشد</p>
    <p class="mb-1">طراحی و توسعه توسط <a href="http://pwp.ir">PwP</a></p>
</footer>
</body>
<script type="text/javascript" src="/frontend/js/jquery-3.3.1.min.js"></script>
@yield('scripts')
<script type="text/javascript" src="/frontend/js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="/frontend/js/bootstrap.js"></script>
<script type="text/javascript" src="/frontend/js/popper.min.js"></script>
<script type="text/javascript" src="/frontend/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/frontend/js/main.js"></script>

</html>
