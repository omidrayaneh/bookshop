@extends('admin.layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{asset('/admins/dist/css/dropzone.css')}}">
@endsection

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>کتاب ها</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
                <li class="active">داشبرد</li>
            </ol>
        </section>
        <section class="content" id="app">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right">ویرایش کتاب {{$book->title}}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form method="post" action="/admin/books/{{$book->id}}">
                                @csrf
                                <input type="hidden" name="_method" value="PATCH">

                                <div class="form-group">
                                    <label for="title">نام</label>
                                    <input type="text" name="title" class="form-control" value="{{$book->title}}" placeholder="نام کتاب را وارد کنید...">
                                </div>
                                {{--<div class="form-group">
                                    <label for="slug">نام مستعار</label>
                                    <input type="text" name="slug" class="form-control" value="{{$book->slug}}"  placeholder="نام مستعار کتاب را وارد کنید...">
                                </div>--}}
                                <div class="form-group">
                                    <label for="category_id">دسته اصلی</label>
                                    <select name="category_id" id="" class="form-control">
                                        @foreach($categories as $category_date)
                                            <option value="{{$category_date->id}}" @if($book->category_id == $category_date->id) selected @endIf >{{$category_date->title}}</option>
                                            @if(count($category_date->childrenRecursive)>0)
                                                @include('admin.partials.edit_book_category',['categories'=>$category_date->childrenRecursive,'level'=>1,'selected_category'=>$category])
                                            @endif
                                            {{--{{  $category_date->id}}--}}
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="author_id">نویسنده ها</label>
                                    <select name="author_id" id="" class="form-control">
                                        @foreach($authors as $author_data)
                                            <option value="{{$author_data->id}}" @if($book->authors->id == $author_data->id) selected @endIf >{{$author_data->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="narrator_id">گوینده ها</label>
                                    <select name="narrator_id" id="" class="form-control">
                                        @foreach($narrators as $narrator_data)
                                            <option value="{{$narrator_data->id}}" @if($book->narrators->id == $narrator_data->id) selected @endIf >{{$narrator_data->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="publisher_id">ناشر ها</label>
                                    <select name="publisher_id" id="" class="form-control">
                                        @foreach($publishers as $publisher_data)
                                            <option value="{{$publisher_data->id}}" @if($book->publishers->id == $publisher_data->id) selected @endIf >{{$publisher_data->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="free">وضعیت</label>
                                    <div>
                                        <input type="radio" name="free" value="0" @if($book->free == 0) checked @endif> <span class="margin-l-10">غیر رایگان</span>
                                        <input type="radio" name="free" value="1" @if($book->free == 1) checked @endif> <span>رایگان</span>
                                    </div>
                                </div>
                                {{--<div class="form-group">
                                    <label for="free">وضعیت برای عموم</label>
                                    <div>
                                        <input type="radio" name="free4user" value="0" @if($book->free == 0) checked @endif> <span class="margin-l-10">غیر رایگان</span>
                                        <input type="radio" name="free4user" value="1" @if($book->free == 1) checked @endif> <span>رایگان</span>
                                    </div>
                                </div>--}}
                                <div class="form-group">
                                    <label>وضعیت نشر</label>
                                    <div>
                                        <input type="radio" name="status" value="0" @if($book->status == 0) checked @endif> <span class="margin-l-10">منتشر نشده</span>
                                        <input type="radio" name="status" value="1" @if($book->status == 1) checked @endif> <span>منتشر شده</span>
                                    </div>
                                </div>
                               {{-- <div class="form-group">
                                    <label>اختصاصی</label>
                                    <div>
                                        <input type="radio" name="vip" value="0" @if($book->vip == 0) checked @endif> <span class="margin-l-10">عمومی</span>
                                        <input type="radio" name="vip" value="1" @if($book->vip == 1) checked @endif> <span>مشترکین</span>
                                    </div>
                                </div>--}}
                                <div class="form-group">
                                    <label>قیمت</label>
                                    <input type="number" name="price"  value="{{$book->price}}" class="form-control" placeholder="قیمت کتاب را وارد کنید...">
                                </div>
                                <div class="form-group">
                                    <label>درصد تخفیف</label>
                                    <input type="number" name="discount_percent" value="{{$book->discount_percent}}" class="form-control" placeholder="قیمت ویژه کتاب را وارد کنید...">
                                </div>
                               {{-- <div class="form-group">
                                    <label>توضیحات</label>
                                    <textarea id="description" type="text" name="description" class="ckeditor form-control" placeholder="توضیحات کتاب را وارد کنید...">{{$book->description}}</textarea>
                                </div>--}}
                                <div class="form-group">
                                    <label for="photo">گالری تصاویر</label>
                                    <input type="hidden" name="photo_id[]" id="book-photo">
                                    <div id="photo" class="dropzone"></div>
                                    <div class="row">
                                        @foreach($book->photos as $photo)
                                            <div class="col-sm-3" id="updated_photo_{{$photo->id}}">
                                                <img class="img-responsive" src="{{$photo->path}}">
                                                <button type="button" class="btn btn-danger" onclick="removeImages({{$photo->id}})">حذف</button>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="voice">گالری صوت</label>
                                    <input type="hidden" name="voice_id[]" id="book-voice">
                                    <div id="voice" class="dropzone"></div>

                                    <div class="row">
                                        @foreach($book->voices as $voice)
                                            <div class="col-sm-6" id="updated_voice_{{$voice->id}}">
                                                <audio controls>
                                                    <source src="{{$voice->path}}" type="audio/mp3">
                                                    <source src="{{$voice->path}}" type="audio/ogg">
                                                </audio>
                                                <button type="button" class="btn btn-danger" onclick="removeVoices({{$voice->id}})">حذف</button>
                                                <input   type="checkbox" name="ids[]" value="{{$voice->id}}" @if($voice->free == 1) checked @endif > رایگان
                                            </div>

                                        @endforeach
                                    </div>
                                </div>
                              {{--  <div class="form-group">
                                    <label>عنوان سئو</label>
                                    <input type="text" name="meta_title"  value="{{$book->meta_title}}" class="form-control" placeholder="عنوان سئو را وارد کنید...">
                                </div>
                                <div class="form-group">
                                    <label>توضیحات سئو</label>
                                    <textarea type="text" name="meta_desc" class="form-control" placeholder="توضیحات سئو را وارد کنید...">{{$book->meta_desc}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>کلمات کلیدی سئو</label>
                                    <input type="text" name="meta_keywords" value="{{$book->meta_keywords}}" class="form-control" placeholder="کلمات کلیدی سئو را وارد کنید...">
                                </div>--}}
                                <button type="submit" onclick="bookGallery()" class="btn btn-success pull-left">ذخیره</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection

@section('script-vuejs')
    <script src="{{asset('admins/js/app.js')}}"></script>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/admins/dist/js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{asset('/admins/plugins/ckeditor/ckeditor.js')}}"></script>
    <script>
        Dropzone.autoDiscover = false;
        var photosGallery = []
        var voicesGallery=[]
        var photos = [].concat({{$book->photos->pluck('id')}})
        var voices = [].concat({{$book->voices->pluck('id')}})
        var drop_photo = new Dropzone('#photo', {
            addRemoveLinks: true,
            maxFiles: 1,
            acceptedFiles: ".jpg",
            maxFilesize: 10,
            dictDefaultMessage:"افزودن عکس کتاب",
            dictFileTooBig:"اندازه عکس بیشتر از حجم پیش فرض می باشد",// image size error message
            dictInvalidFileType:"فایل انتخابی برای این قسمت مناسب نمی باشد",// file type error message
            dictCancelUpload:" لغو آپلود عکس",//cancel error message
            dictCancelUploadConfirmation:"آیا می خواهید آپلود را متوقف کنید؟", //cancel conform
            dictRemoveFile:"حذف عکس",// remove file
            url: "{{ route('photos.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                photosGallery.push(response.photo_id)
            }
        });
        bookGallery = function(){
            document.getElementById('book-photo').value = photosGallery.concat(photos)
        }
        removeImages = function(id){
            var index = photos.indexOf(id)
            photos.splice(index, 1);
            document.getElementById('updated_photo_' + id).remove();
        }
        var drop_voice = new Dropzone('#voice', {
            addRemoveLinks: true,
            acceptedFiles: ".mp3",
            maxFilesize: 256,
            paramName: "file",
            acceptedMimeTypes: null,
            acceptParameter: null,
            enqueueForUpload: true,
            dictDefaultMessage:"افزودن صوت کتاب",
            dictFileTooBig:"اندازه صوت بیشتر از حجم پیش فرض می باشد",// image size error message
            dictInvalidFileType:"فایل انتخابی برای این قسمت مناسب نمی باشد",// file type error message
            dictCancelUpload:" لغو آپلود صوت",//cancel error message
            dictCancelUploadConfirmation:"آیا می خواهید آپلود را متوقف کنید؟", //cancel conform
            dictRemoveFile:"حذف صوت",// remove file
            url: "{{ route('voices.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                voicesGallery.push(response.voice_id)
            }
        });
        temp=bookGallery;
        bookGallery = function(){
            console.log(temp())
            document.getElementById('book-voice').value = voicesGallery.concat(voices)
        }
        removeVoices = function(id){
            var index = voices.indexOf(id)
            voices.splice(index, 1);
            document.getElementById('updated_voice_' + id).remove();
        }
        CKEDITOR.replace('description',{
            customConfig: 'config.js',
            toolbar: 'simple',
            language: 'fa',
            removePlugins: 'cloudservices, easyimage'
        })
    </script>

@endsection