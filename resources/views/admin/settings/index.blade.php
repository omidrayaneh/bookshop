@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>تنظیم ها</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
                <li class="active">داشبرد</li>
            </ol>
        </section>
        <section class="content">
            <section class="content">
                <div class="box box-info">
                    <div class=" text-left">
                        <a class="btn btn-app" href="{{route('settings.create')}}">
                            <i class="fa fa-plus"></i> جدید
                        </a>
                    </div>
                    <!-- /.box-header -->
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="box-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                <div>{{session('success')}}</div>
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                <div>{{session('error')}}</div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th class="text-right"> آخرین محصول ها</th>
                                    <th class="text-right">پر بازدیدها</th>
                                    <th class="text-right">پر فروش ها</th>

                                </tr>
                                </thead>
                                <tbody>
                                @php $i=1 @endphp
                                @foreach($settings as $setting)
                                    <tr>
                                        <td>{{$i}}</td>
                                        @if($setting->last==1)
                                            <td><span class="label label-success"> نمایش داده شود</span></td>
                                        @else
                                            <td><span class="label label-danger">نمایش داده نشود</span></td>
                                        @endif
                                        @if($setting->visit==1)
                                            <td><span class="label label-success"> نمایش داده شود</span></td>
                                        @else
                                            <td><span class="label label-danger">نمایش داده نشود</span></td>
                                        @endif
                                        @if($setting->order==1)
                                            <td><span class="label label-success"> نمایش داده شود</span></td>
                                        @else
                                            <td><span class="label label-danger">نمایش داده نشود</span></td>
                                        @endif
                                        <td class="text-center">
                                            <div class="display-inline-block">
                                                <form method="post" action="/admin/settings/{{$setting->id}}">
                                                    <a href="{{route('settings.edit',$setting->id,compact($setting))}}" class="btn btn-warning" >ویرایش</a>
                                                    @csrf
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button type="submit"  class="btn btn-danger " >حذف</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @php $i++; @endphp
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                        <!-- /.table-responsive -->
                    </div>
                </div>
            </section>
        </section>

    </div>
@endsection




