@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>سفارش ها</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right">لیست سفارش ها </h3>
                    <div class=" text-left">

                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th class="text-center">ردیف</th>
                                <th class="text-center">شناسه</th>
                                <th class="text-right">مبلغ</th>
                                <th class="text-right">وضعیت</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php $i=1 @endphp
                            @foreach($orders as $order)
                                <tr>
                                    <td class="text-right">{{$i}}</td>
                                    <td class="text-center">{{$order->id}}</td>
                                    <td class="text-right">{{$order->amount}}</td>
                                    @if($order->status==1)
                                        <td><span class="label label-success">تسویه شده</span></td>
                                    @else
                                        <td><span class="label label-danger">تسویه نشده</span></td>
                                    @endif
                                </tr>
                                @php $i++; @endphp
                            @endforeach
                            </tbody>
                        </table>
                        <div class="col-md-12" style="text-align: center">{{$orders->links()}}</div>

                    </div>
                    <!-- /.table-responsive -->
                </div>
            </div>
        </section>

    </div>
@endsection