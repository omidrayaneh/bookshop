@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>دسته بندی ها</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right">ایجاد دسته بندی جدید</h3>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form method="post" action="/admin/categories">
                                @csrf
                                <div class="form-group">
                                    <label for="title">نام</label>
                                    <input type="text" name="title" class="form-control" placeholder="عنوان دسته بندی ...">
                                </div>
                               {{-- <div class="form-group">
                                    <label for="slug">نام مستعار</label>
                                    <input type="text" name="slug" class="form-control" placeholder="نام مستعار محصول ...">
                                </div>--}}
                                <div class="form-group">
                                    <label for="category_parent">دسته اصلی</label>
                                    <select name="category_parent" id="" class="form-control">
                                        <option value="">انتخاب کنید</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                            @if(count($category->childrenRecursive)>0)
                                                @include('admin.partials.category',['categories'=>$category->childrenRecursive,'level'=>1])
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>وضعیت انتشار در اول سایت</label>
                                    <div>
                                        <input type="radio" name="first_page" value="0" checked> <span class="margin-l-10">منتشر نشده</span>
                                        <input type="radio" name="first_page" value="1"> <span>منتشر شده</span>
                                    </div>
                                </div>
                               {{-- <div class="form-group">
                                    <label for="meta_title">عنوان ...</label>
                                    <input type="text" name="meta_title" class="form-control" placeholder="عنوان دسته بندی ...">
                                </div>
                                <div class="form-group">
                                    <label for="meta_desc">توضیحات ...</label>
                                    <textarea type="text" name="meta_desc" class="form-control" placeholder="توضیحات دسته بندی ..."></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="meta_keywords">کلمات کلیدی ...</label>
                                    <input type="text" name="meta_keywords" class="form-control" placeholder="کلمات کلیدی دسته بندی ...">
                                </div>--}}
                                <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection