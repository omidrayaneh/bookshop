@extends('layouts.master')
@section('title')
    حساب کاربری
@endsection

@section('content')
    <header class="head-gradiant " >
        <div class="container">
            <h1 class="mt-5"></h1>
            <p class="tagline"></p>
            <div class="py-5">
            </div>
        </div>
    </header>
    <div class="container-fluid light-bg mb-2 text-center">
        <div class="row p-3 mt-5">
            <div class="col-12 text-center">
                <p></p>
            </div>

        </div>
    </div>

    <div class="container-fluid light-bg mb-2 text-center">
        <div class="row p-3 mt-5">
            <div class="col-12 text-center">
                <p></p>
            </div>

        </div>
    </div>
    @if(Cart::count()!=0)
        <div class="container light-bg mb-2">
            <div class="row p-3 mt-5">
                <div class="col-12 text-center">
                    <h2 class="title">سبد خرید</h2>
                </div>
            </div>
        </div>
    @else
        <div class="container light-bg mb-2">
            <div class="row p-3 mt-5">
                <div class="col-12 text-center">
                    <h3 class="title">اعتبار شما به اتمام رسیده است</h3>
                    <button onclick="location.href='{{url('/')}}'" type="button">بازکشت به فروشگاه</button>

                </div>
            </div>
        </div>
    @endif
@endsection