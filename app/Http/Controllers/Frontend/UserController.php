<?php

namespace App\Http\Controllers\Frontend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $users=User::all();
        $user=new User();

        $validator=Validator::make($request->all(),[
            'email'=>'required|unique:users',
            'name'=>'required',
            'last_name'=>'required',
            'password'=>'required',
        ],[
            'name.required'=>'نام را وارد کنید',
            'last_name.required'=>'نام خانوادگی را وارد کنید',
            'email.unique'=>'این ایمیل قبلا ثبت شده است',
            'email.required'=>'ایمیل را وارد کنید',
            'password.required'=>'رمز عبور را وارد کنید',
        ]);
        if($validator->fails()){
            return redirect('/register')->withErrors($validator)->withInput();
        }else{
            if( count($users)==0){
                $user->is_admin=1;
            }
            else{
                $user->is_admin=0;
            }
            $user->name=$request->input('name');
            $user->last_name=$request->input('last_name');
            $user->email=$request->input('email');
            $user->password=Hash::make($request->input('password'));

            $user->save();
            Session::flash('success', 'ثبت نام شما با موفقیت انجام شد');
            return redirect('/');
        }




    }


}
