<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index(){
        $orders=Order::paginate(10);
        return view('admin.orders.index',compact(['orders']));
    }


}
