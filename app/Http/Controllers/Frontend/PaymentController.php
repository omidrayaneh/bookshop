<?php

namespace App\Http\Controllers\Frontend;

use App\Order;
use App\Order_Item;
use App\Payment;
use App\Subscription;
use App\Vip;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PaymentController extends Controller
{
    public function verify(Request $request,$id){

        $redirect='';
        $order =Order::whereId($id)->first();

        $payment = new Payment($order->amount);
        $result = $payment->verifyPayment($request->Authority, $request->Status);
        if ( $result){

            $order=Order::findOrFail($id);
            $order->status=1;
            $order->save();

            $newPayment=new Payment($order->amount);

            $newPayment->authority=$request->Authority;
            $newPayment->status=$request->Status;
            $newPayment->RefID=$result->RefID;
            $newPayment->order_id=$id;
            $newPayment->save();

            $order_item =Order_Item::where('order_id',$id)->first();
            $sub=Subscription::whereId($order_item->subscription_id)->first();
            if ($sub!=null){
                DB::table('vips')->where('user_id',Auth::user()->id)->update(['status' => 0]);
                $newVip=new Vip();
                $newVip->price=$sub->price;
                $newVip->status=$sub->limited_book;
                $newVip->user_id=Auth::user()->id;
                $newVip->subscription_id=$sub->id;
                $current_time=Carbon::now();
                $newVip->start_day=$current_time;
                $end_time=Carbon::now();
                $newVip->end_day=$end_time->addMonth($sub->period);
                $newVip->save();

                $redirect='/profile';
                Session::flash('success', 'پرداخت شما با موفقیت انجام شد،اطلاعت خرید در پروفایل کاربری شما قابل مشاهده است');
            }
            else{
                $redirect='/';

                $b=  DB::table('cart_models')->delete();
                Session::flash('success', 'پرداخت شما با موفقیت انجام شد');
            Cart::destroy();
            }
            return redirect($redirect);
        }else{
            Session::flash('error', 'در پرداخت شما خطایی به وجود آمد');
            return redirect('/cart');
        }
    }


    public function index()
    {
        $payments=Payment::paginate(10);
        return view('admin.payments.index',compact(['payments']));
    }
}
