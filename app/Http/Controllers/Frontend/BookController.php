<?php

namespace App\Http\Controllers\Frontend;

use App\Author;
use App\Book;
use App\Category;
use App\Comment;
use App\Voice;
use foo\bar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;
use function Sodium\add;
use Validator;

class BookController extends Controller
{
    public function addComment(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'title'=>'required',
            'name'=>'required',
             'description'=>'required',
             'email'=>'required',
        ],[
            'title.required'=>'موضوع خود را وارد کنید',
            'name.required'=>' نام خود را وارد کنید',
            'description.required'=>'متن خود را وارد کنید',
            'email.required'=>'ایمیل خود را وارد کنید',

        ]);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        else {
            $newComment = new Comment();
            $newComment->title = $request->input('title');
            $newComment->description = $request->input('description');
            $newComment->email = $request->input('email');
            $newComment->book_id = $request->id;
            $newComment->status = 0;
            $newComment->name = $request->input('name');
            if (Auth::check()) {
                $newComment->user_id = Auth::user()->id;
            }
            $newComment->save();
            Session::flash('success', 'نظر شما در مورد این کتاب ثبت گردید');
            return back();
        }

    }

    public function search(Request $request)
    {

        $q = $request->input('search');
        $books = Book::with('photos', 'authors')
            ->where('title', 'LIKE', '%' . $q . '%')
            ->where('status',1)
            ->get();
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
        $count = count($categories);
        $categories1 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset(0)
            ->limit($count / 2)->get();
        $categories2 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset($count / 2)
            ->limit($count / 2)->get();
        return view('frontend.search.index', compact(['categories1', 'categories2', 'books','q']));

    }

    public function searchByAuthor(Request $request)
    {

        $books = Book::with('photos', 'authors')->where('author_id', $request->author_id)
            ->where('status',1)
            ->get();
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
        $count = count($categories);
        $categories1 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset(0)
            ->limit($count / 2)->get();
        $categories2 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset($count / 2)
            ->limit($count / 2)->get();
        return view('frontend.search.index', compact(['categories1', 'categories2', 'books']));
    }

    public function searchByNarrator(Request $request)
    {
        $books = Book::with('photos', 'authors')->where('narrator_id', $request->narrator_id)
            ->where('status',1)
            ->get();
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
        $count = count($categories);
        $categories1 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset(0)
            ->limit($count / 2)->get();
        $categories2 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset($count / 2)
            ->limit($count / 2)->get();
        return view('frontend.search.index', compact(['categories1', 'categories2', 'books']));
    }

    public function searchByPublisher(Request $request)
    {
        $books = Book::with('photos', 'authors')->where('publisher_id', $request->publisher_id)
            ->where('status',1)
            ->get();
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
        $count = count($categories);
        $categories1 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset(0)
            ->limit($count / 2)->get();
        $categories2 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset($count / 2)
            ->limit($count / 2)->get();
        return view('frontend.search.index', compact(['categories1', 'categories2', 'books']));
    }

    public function searchByCategory(Request $request)
    {
        $id = $request->id;

        $category = Category::with('childrenRecursive')
            ->where('parent_id', $id)
            ->first();

        if (!isset($category))
            return view('errors.404');

        $books = Book::with('photos', 'authors')->where('category_id', $category->id)
            ->where('status',1)->get();
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
        $count = count($categories);
        $categories1 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset(0)
            ->limit($count / 2)->get();
        $categories2 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset($count / 2)
            ->limit($count / 2)->get();
        return view('frontend.search.index', compact(['categories1', 'categories2', 'books']));

    }


    public function book($id)
    {



        if (Auth::check()) {
            $book = Book::with('photos', 'voices', 'authors', 'publishers', 'narrators', 'comments')->whereId($id)->first();


            $comments = Comment::where([['book_id', $id], ['status', 1]])->get();
            $voices = new Voice();

            $categories = Category::all();
            $count = count($categories);

            $categories1 = Category::with('childrenRecursive')
                ->where('parent_id', null)->offset(0)
                ->limit($count / 2)->get();
            $categories2 = Category::with('childrenRecursive')
                ->where('parent_id', null)->offset($count / 2)
                ->limit($count / 2)->get();
            try {
                $book_purchased = DB::table('orders')
                    ->leftJoin('order__items', 'orders.id', '=', 'order__items.order_id')
                    ->leftJoin('books', 'order__items.book_id', '=', 'books.id')
                    ->select('order__items.*', 'orders.*', 'books.*')
                    ->where('orders.user_id', Auth::user()->id)
                    ->where('orders.status', 1)
                    ->where('books.id', $id)
                    ->get();
            } catch (\Exception $e) {
                return view('errors.404');
            }

            $not_free_voice = [];
            $free_voice[] = [];
            $j = 0;
            $i = 0;
            if (!isset($book->voices)) {
                return view('errors.404');
            }


            if (count($book_purchased) > 0) {
                foreach ($book->voices as $NF_voice) {
                    if ($NF_voice->free == 0) {
                        $not_free_voice[$i] = $NF_voice;
                    }
                    $i++;
                }
            }
            foreach ($book->voices as $F_voice) {
                if ($F_voice->free == 1) {
                    $free_voice[$j] = $F_voice;
                    $j++;
                }
            }


            return view('frontend.books.index', compact(['categories1', 'categories2', 'book', 'voices', 'comments', 'book_purchased', 'not_free_voice', 'free_voice']));
        }
        else{

            $book = Book::with('photos', 'voices', 'authors', 'publishers', 'narrators', 'comments')->whereId($id)->where('status',1)->first();


            $comments = Comment::where([['book_id', $id], ['status', 1]])->get();
            $voices = new Voice();

            $categories = Category::all();
            $count = count($categories);

            $categories1 = Category::with('childrenRecursive')
                ->where('parent_id', null)->offset(0)
                ->limit($count / 2)->get();
            $categories2 = Category::with('childrenRecursive')
                ->where('parent_id', null)->offset($count / 2)
                ->limit($count / 2)->get();
            $free_voice[] = [];
            $j = 0;
            $i = 0;
            if (!isset($book->voices)) {
                return view('errors.404');
            }

            foreach ($book->voices as $F_voice) {
                if ($F_voice->free == 1) {
                    $free_voice[$j] = $F_voice;
                    $j++;
                }
            }
            if (!count($book)==0){
            $visit=$book->visit;
            $d= DB::table('books')->whereId($book->id)->update(['visit' => $visit+1]);
             }
            return view('frontend.books.index', compact(['categories1', 'categories2', 'book', 'voices', 'comments', 'free_voice']));
        }
    }

    public function PurchaseApi($id)
    {
        return $id;
    }
}
