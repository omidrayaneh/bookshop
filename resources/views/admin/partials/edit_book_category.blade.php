@if(isset($selected_category))
    @foreach($categories as $sub_category)
        <option value="{{$sub_category->id}}" @if($selected_category->id==$sub_category->id) selected @endIf>{{str_repeat(' --- ',$level)}}{{$sub_category->title}}</option>
        @if(count($sub_category->childrenRecursive)>0)
            @include('admin.partials.edit_book_category',['categories'=>$sub_category->childrenRecursive,'level'=>$level+1,'selected_category'])
        @endif
    @endforeach
@else
    @foreach($categories as $sub_category)
        <option value="{{$sub_category->id}}">{{str_repeat(' --- ',$level)}}{{$sub_category->title}}</option>
        @if(count($sub_category->childrenRecursive)>0)
            @include('admin.partials.edit_book_category',['categories'=>$sub_category->childrenRecursive,'level'=>$level+1])
        @endif
    @endforeach
@endif