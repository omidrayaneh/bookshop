<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::paginate(10);
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'required',
             'last_name' => 'required',
             'email'=>'required|unique:users',
             'password'=>'required|min:8',
             're_password'=>'sometimes|required_with:password',
        ],[
            'name.required'=>'نام کاربر نمی توتند خالی باشد',
            'last_name.required'=>'نام خانوادگی کاربر نمی توتند خالی باشد',
            'email.required'=>'ایمیل نمی توتند خالی باشد',
            'email.unique'=>'این ایمیل قبلا ثبت شده است',
            'password.required'=>'رمز عبور خالیست',
            'password.min'=>'تعداد رمز عبور کمتر از 8 کاراکتر است',
            're_password.required_with'=>'تکرار رمز عبور اشتباه است',

        ]);
        if($validator->fails()){
            return redirect('/admin/users/create')->withErrors($validator)->withInput();
        }
        else {
            $newUser = new User();
            $newUser->name=$request->input('name');
            $newUser->last_name=$request->input('last_name');
            $newUser->email=$request->input('email');
            $newUser->password=Hash::make($request->input('name'));
            $newUser->status=$request->input('status');
            $newUser->is_admin=$request->input('role_status');
            $newUser->save();
            Session::flash('success', 'کاربر  با موفقیت اضافه شد.');
            return redirect('/admin/users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user =User::findOrfail($id);
        return view('admin.users.edit', compact(['user']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'last_name' => 'required',
            'email'=>'required|unique:users,name,'.$id,
            're_password'=>'sometimes|required_with:password',
        ],[
            'name.required'=>'نام کاربر نمی توتند خالی باشد',
            'last_name.required'=>'نام خانوادگی کاربر نمی توتند خالی باشد',
            'email.required'=>'ایمیل نمی توتند خالی باشد',
            'email.unique'=>'این ایمیل قبلا ثبت شده است',
            're_password.required_with'=>'تکرار رمز عبور اشتباه است',

        ]);
        if($validator->fails()){
            return redirect('/admin/users/'.$id.'/edit')->withErrors($validator)->withInput();
        }
        else {
            $user =User::findOrFail($id);
            $user->name=$request->input('name');
            $user->last_name=$request->input('last_name');
            $user->email=$request->input('email');
            if ($request->input('password')){
                $user->password=Hash::make($request->input('password'));
            }
            $user->status=$request->input('status');
            $user->is_admin=$request->input('role_status');
            $user->save();
            Session::flash('success', 'کاربر  با موفقیت ویرایش شد.');
            return redirect('/admin/users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        Session::flash('error', 'کاربر با موفقیت حذف شد.');
        return redirect('/admin/users');
    }


}
