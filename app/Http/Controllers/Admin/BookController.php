<?php

namespace App\Http\Controllers\Admin;

use App\Author;
use App\Book;
use App\Category;
use App\Narrator;
use App\Publisher;
use App\Voice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;
class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books =Book::paginate(10);
        return view('admin.books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
        $authors=Author::where('status',1)->get();
        $publishers=Publisher::where('status',1)->get();
        $narrators=Narrator::where('status',1)->get();
        return view('admin.books.create', compact(['categories','authors','publishers','narrators']));
    }
    public function generateSku()
    {
        $number = mt_rand(1000, 99999);
        if ($this->checkSku($number)) {
            return $this->generateSku();
        }
        return (string)$number;
    }
    public function checkSku($number)
    {
        return Book::where('sku', $number)->exists();
    }
    function makeSlug($string)
    {
        return preg_replace('/\s+/u', '-', trim($string));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:books',
          //  'slug' => 'required|unique:books',
            'price' => 'numeric',
            'photo_id'=>'required|array',
            'photo_id.*'=>'integer',
            'voice_id'=>'required|array',
            'voice_id.*'=>'integer',

        ], [
            'title.required' => 'نام کالا خالیست',
            'title.unique' => 'این کالا قبلا ثبت شده است',
           // 'slug.required' => ' نام مستعار کالا خالیست',
           // 'slug.unique' => 'این نام مستعار قبلا ثبت شده است',
            'price.numeric' => 'قیمت کالا خالیست',
            'photo_id.*.integer' => 'عکسی برای این کتاب انتخاب نشده است',
            'voice_id*.integer' => 'صوتی برای این کتاب انتخاب نشده است',

        ]);
        if ($validator->fails()) {
            return redirect('/admin/books/create')->withErrors($validator)->withInput();
        } else {
            $newBook = new Book();
            $newBook->title = $request->title;
            $newBook->sku = $this->generateSKU();
           // $newBook->slug = $this->makeSlug($request->slug);
            $newBook->free = $request->free;
           // $newBook->free4user = $request->free4user;
            $newBook->status = $request->status;
            $newBook->price = $request->price;
            $newBook->visit = 0;
            $newBook->discount_percent = $request->discount_percent;
           // $newBook->description = $request->description;
           /* $newBook->meta_desc = $request->meta_desc;
            $newBook->meta_title = $request->meta_title;
            $newBook->meta_keywords = $request->meta_keywords;*/
           // $newBook->vip = $request->vip;
            $newBook->user_id = Auth::user()->id;
            $newBook->category_id=$request->category_id;
            $newBook->author_id=$request->author_id;
            $newBook->publisher_id=$request->publisher_id;
            $newBook->narrator_id=$request->narrator_id;
            $newBook->save();

            $photos = explode(',', $request->input('photo_id')[0]);
            $newBook->photos()->sync($photos);

            $voices = explode(',', $request->input('voice_id')[0]);
            $newBook->voices()->sync($voices);

            Session::flash('success', 'محصول با موفقیت اضافه شد.');
            return redirect('/admin/books');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // link storage folder to public folder :  php artisan storage:link
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
        $book = Book::with(['photos','voices'])->whereId($id)->first();
        $authors=Author::all();
        $publishers=Publisher::all();
        $narrators=Narrator::all();
        $category = Category::findOrFail($book->category_id);
        return view('admin.books.edit', ['categories'=>$categories,
            'book'=>$book,'category'=>$category,'authors'=>$authors,
            'publishers'=>$publishers,'narrators'=>$narrators]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ids = $request->ids;//checkbox is checked
        $validator = Validator::make($request->all(), [
            'title'=>'required|unique:books,title,'.$id,
           // 'slug' => 'required|unique:books,slug,' . $id,
            'price' => 'numeric',
        ], [
            'title.required' => 'نام کالا خالیست',
            'title.unique' => 'این کالا قبلا ثبت شده است',
           // 'slug.required' => ' نام مستعار کالا خالیست',
            //'slug.unique' => 'این نام مستعار قبلا ثبت شده است',
            'price.numeric' => 'قیمت کالا خالیست',
        ]);
        if ($validator->fails()) {
            return redirect('/admin/books/'.$id.'/edit')->withErrors($validator)->withInput();

        } else {
            $book = Book::findOrFail($id);
            $book->title = $request->title;
            $book->sku = $this->generateSKU();
            //$book->slug = $this->makeSlug($request->slug);
            $book->free = $request->free;
            //$book->free4user = $request->free4user;
            $book->status = $request->status;
            $book->price = $request->price;
            $book->discount_percent = $request->discount_percent;
            /*$book->description = $request->description;
            $book->meta_desc = $request->meta_desc;
            $book->meta_title = $request->meta_title;
            $book->meta_keywords = $request->meta_keywords;*/
            //$book->vip = $request->vip;
            $book->user_id = Auth::user()->id;
            $book->category_id=$request->category_id;
            $book->author_id=$request->author_id;
            $book->publisher_id=$request->publisher_id;
            $book->narrator_id=$request->narrator_id;

            $book->save();

            $photos = explode(',', $request->input('photo_id')[0]);
            $voices = explode(',', $request->input('voice_id')[0]);

            $book->photos()->sync($photos);
            $book->voices()->sync($voices);

            foreach ($voices as $voice_id){
                $all_voice=Voice::findOrfail($voice_id);
                  $all_voice->free=0;
                  $all_voice->save();
            }
            if (isset($ids)){
                foreach ($ids as $v_id){
                    $free_voice=Voice::findOrfail($v_id);
                    $free_voice->free=1;
                    $free_voice->save();
                }
            }

            Session::flash('success', 'کتاب با موفقیت بروز شد.');
            return redirect('/admin/books');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        $book->delete();

        Session::flash('error', 'کتاب با موفقیت حذف شد.');
        return redirect('/admin/books');
    }
}
