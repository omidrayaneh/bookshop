<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CommentController extends Controller
{
    public function index()
    {
        $comments=Comment::with('book')->orderBy('created_at','desc')->paginate(10);
        return view('admin.comments.index',compact('comments'));
    }

    public function actions(Request $request,$id)
    {
        if ($request->has('action')) {
            if ($request->input('action') == 'approved') {
                $comment = Comment::findOrFail($id);
                $comment->status = 1;
                $comment->save();
                Session::flash('success', 'نظر کاربر با موفقیت تایید شد.');
            } else {
                $comment = Comment::findOrFail($id);
                $comment->status = 0;
                $comment->save();
                Session::flash('error', 'نظر کاربر  تایید نشد.');
            }
        }
        return redirect('/admin/comments');
    }
}
