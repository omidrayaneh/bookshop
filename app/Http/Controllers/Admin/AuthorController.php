<?php

namespace App\Http\Controllers\Admin;

use App\Author;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authors=Author::paginate(10);
        return view('admin.authors.index',compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.authors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'required|unique:authors',
        ],[
            'name.required'=>'عنوان نویسنده  خالیست',
            'name.unique'=>'این نویسنده قبلا ثبت شده است',
        ]);
        if($validator->fails()){
            return redirect('/admin/authors/create')->withErrors($validator)->withInput();
        }
        else {
            $authors = new Author();
            $authors->name = $request->name;
            $authors->status = $request->status;
            $authors->save();
            Session::flash('success', 'نویسنده با موفقیت اضافه شد.');
            return redirect('/admin/authors');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author = Author::findOrfail($id);
        return view('admin.authors.edit', compact(['author']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'required|unique:authors,name,'.$id,
        ],[
            'name.required'=>'عنوان نوسنده  خالیست',
            'name.unique'=>'این نویسنده قبلا ثبت شده است',
        ]);
        if($validator->fails()){
            return redirect('/admin/authors/create')->withErrors($validator)->withInput();
        }
        else {
            $author = Author::findOrFail($id);
            $author->name = $request->name;
            $author->status = $request->status;
            $author->save();
            Session::flash('success', 'نویسنده با موفقیت ویرایش شد.');
            return redirect('/admin/authors');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Author::findOrFail($id);
        $author->delete();

        Session::flash('error', 'نویسنده با موفقیت حذف شد.');
        return redirect('/admin/authors');
    }
}
