$(document).ready(function(){

    /////////////////////////////////////////////////
    // .............. color-box .............. //
    /////////////////////////////////////////////////
    $('.inline').colorbox({
        inline:true,
        width:"450",
    });

    /////////////////////////////////////////////////
    // .............. scroll-top .............. //
    /////////////////////////////////////////////////
    function menuscroll(){
        let navmenu= $('.nav-menu');
        if ($(window).scrollTop() > 50) {
            navmenu.addClass('is-scrolling')
        }else{
            navmenu.removeClass('is-scrolling')
        }
    }

    /////////////////////////////////////////////////
    // .............. collapse-show .............. //
    /////////////////////////////////////////////////
    menuscroll();
    $(window).on('scroll',menuscroll)

    let navslide= $('#navbar');
    // let navs=$('#navbar ul.navbar-nav li.nav-item');

    navslide.on('show.bs.collapse',function(){
        $(this).parents('.nav-menu').addClass('nav-is-open');
        // navs.children('a').removeClass('active');
    })
    navslide.on('hide.bs.collapse',function(){
        $(this).parents('.nav-menu').removeClass('nav-is-open');
    })

})