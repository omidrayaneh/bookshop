<?php
namespace App\Http\Controllers\Admin;
use App\Book;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->paginate(5);
        return view('admin.categories.index', compact(['categories']));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
        return view('admin.categories.create', compact(['categories']));
    }
    public function generateSku()
    {
        $number = mt_rand(1000, 99999);
        if ($this->checkSku($number)) {
            return $this->generateSku();
        }
        return (string)$number;
    }
    public function checkSku($number)
    {
        return Category::where('sku', $number)->exists();
    }
    function makeSlug($string)
    {
        //$string = strtolower($string);
        //$string = str_replace(['؟', '?'], '', $string);
        return preg_replace('/\s+/u', '-', trim($string));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'title'=>'required|unique:categories',
           /* 'slug' => 'required|unique:categories',
            'meta_title'=>'required',
            'meta_desc'=>'required',
            'meta_keywords'=>'required',*/
        ],[
            'title.required'=>'عنوان گروه  خالیست',
            'title.unique'=>'این گروه قبلا ثبت شده است',
            /*'slug.required' => ' نام مستعار  خالیست',
            'slug.unique' => 'این نام مستعار قبلا ثبت شده است',
            'meta_title.required'=>'عنوان متای گروه خالیست',
            'meta_desc.required'=>'توضیحات گروه خالیست',
            'meta_keywords.required'=>'کلید واژه گروه خالیست',*/
        ]);
        if($validator->fails()){
            return redirect('/admin/categories/create')->withErrors($validator)->withInput();
        }
        else {
            $category = new Category();
            $category->title = $request->input('title');
            $category->first_page = $request->input('first_page');
            $category->sku = $this->generateSKU();
           //$category->slug = $this->makeSlug($request->slug);
            $category->parent_id = $request->input('category_parent');
           // $category->meta_title = $request->input('meta_title');
           // $category->meta_desc = $request->input('meta_desc');
           // $category->meta_keywords = $request->input('meta_keywords');
            $category->user_id = Auth::user()->id;
            $category->save();
            Session::flash('success', 'دسته بندی با موفقیت اضافه شد.');
            return redirect('/admin/categories');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
        $category = Category::findOrfail($id);
        return view('admin.categories.edit', ['categories' => $categories, 'category' => $category]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'title'=>'required|unique:categories,title,'.$id,
            //'meta_title'=>'required',
           // 'slug' => 'required|unique:categories,slug,' . $id,
           // 'meta_desc'=>'required',
           // 'meta_keywords'=>'required',
        ],[
            'title.required'=>'عنوان گروه  خالیست',
            'title.unique'=>'این گروه قبلا ثبت شده است',
           // 'slug.required' => ' نام مستعار  خالیست',
           // 'slug.unique' => 'این نام مستعار قبلا ثبت شده است',
           // 'meta_title.required'=>'عنوان متای گروه خالیست',
          //  'meta_desc.required'=>'توضیحات گروه خالیست',
           // 'meta_keywords.required'=>'کلید واژه گروه خالیست',
        ]);
        if($validator->fails()){
            return redirect('/admin/categories/'.$id.'/edit')->withErrors($validator)->withInput();
        }
        else {
            $book=Book::where('category_id', $id)
                ->where('status',1)
                ->get();
            $category = Category::findOrFail($id);
            $category->title = $request->input('title');
            if (!$book->count()){
                Session::flash('error', 'به این دسته بندی کتابی تعلق نگرفته است، گزینه "وضعیت انتشار در اول سایت" نمی تواند فعال باشد.');
                $category->first_page =0;
            }else{
                $category->first_page = $request->input('first_page');
            }

            $category->sku = $this->generateSKU();
            //  $category->slug = $this->makeSlug($request->slug);
            $category->parent_id = $request->input('category_parent');
            //  $category->meta_title = $request->input('meta_title');
            // $category->meta_desc = $request->input('meta_desc');
            //  $category->meta_keywords = $request->input('meta_keywords');
            $category->user_id = Auth::user()->id;
            $category->save();

            Session::flash('success', 'دسته بندی با موفقیت ویرایش شد.');
            return redirect('/admin/categories');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::with('childrenRecursive')->where('id', $id)->first();
        $books=Book::where('category_id',$id)->first();
        if (count($books) > 0) {
            Session::flash('error', ' دسته بندی ' . $category->title . ' دارای محصول می باشد و حذف آن امکان پذیر نیست.');
            return redirect('/admin/categories');
        }
        if (count($category->childrenRecursive) > 0) {
            Session::flash('error', ' دسته بندی ' . $category->title . ' دارای زیر دسته می باشد و حذف آن امکان پذیر نیست.');
            return redirect('/admin/categories');
        }
        $category->delete();
        Session::flash('error', 'دسته بندی با موفقیت حذف شد.');
        return redirect('/admin/categories');
    }
}