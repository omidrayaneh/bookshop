<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;
use SoapClient;

class Payment extends  Model
{
    private $MerchantID;
    private $Amount;
    private $Description;
    private $CallbackURL;

    public function __construct($amount,$orderId=null)
    {
        $bank=Bank::where('status',1)->first();
        if (count($bank)==0){
            Session::flash('error', 'مشکلی در پرداخت بوجود آمد، با پشتیبانی تماس بگیرید');
            return redirect('/');
        }
        $this->MerchantID = $bank->merchantID; //Required
        $this->Amount = $amount; //Amount will be based on Toman - Required
        $this->Description = $bank->description;// Required
        $this->CallbackURL = 'http://bookshop.test:10/payment-verify/'.$orderId; // Required
        //$this->CallbackURL = 'http://masneed.com/payment-verify/'.$orderId; // Required

    }

    public function doPayment()
    {
        try{
            $client = new SoapClient('https://sandbox.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
            // change this=>'https://sandbox.zarinpal.com/pg/services/WebGate/wsdl' to this => 'https://www.zarinpal.com/pg/services/WebGate/wsdl'
            $result = $client->PaymentRequest(
                [
                    'MerchantID' =>$this->MerchantID,
                    'Amount' =>$this->Amount,
                    'Description' =>$this->Description,
                    'CallbackURL' =>$this->CallbackURL,
                ]
            );
        }catch (\Exception $e){
            Session::flash('error', 'در پرداخت شما خطایی به وجود آمد');
            return redirect('/cart');
        }

        return $result;
    }

    public function verifyPayment($authority,$status)
    {
        if ($status == 'OK') {

            $client = new SoapClient('https://sandbox.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
            // change this=>'https://sandbox.zarinpal.com/pg/services/WebGate/wsdl' to this => 'https://www.zarinpal.com/pg/services/WebGate/wsdl'

            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $this->MerchantID,
                    'Authority' => $authority,
                    'Amount' => $this->Amount,
                ]
            );
            return $result;
        }
        else{
            return false;
        }

    }

    public function order()
    {
        return $this->hasOne(Order::class);
    }

}
