<?php

namespace App\Http\Controllers\Admin;

use App\Voice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class VoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function upload(Request $request)
    {
        $uploadFile=$request->file('file');
       // $filename=time().$uploadFile->getClientOriginalName();
        $extension = $request->file('file')->extension();
        $filename=time().'book-voice'.'.'.$extension;
        $original_name=$uploadFile->getClientOriginalName();
        Storage::disk('local')->putFileAs(
            'public/voices',$uploadFile,$filename
        );
        $voice=new Voice();
        $voice->original_name=$original_name;
        $voice->path=$filename;
        $voice->user_id=Auth::user()->id;
        $voice->save();
        return response()->json([
            'voice_id'=>$voice->id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
