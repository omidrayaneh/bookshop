<?php
namespace App\Http\Controllers\Admin;
use App\Photo;
use App\Slider;
use App\Voice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
class PhotoController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
    }
    public function upload(Request $request)
    {
        $uploadFile=$request->file('file');
       // $filename=time().$uploadFile->getClientOriginalName();
        $extension = $request->file('file')->extension();
        $filename=time().'book-photo'.'.'.$extension;
        $original_name=$uploadFile->getClientOriginalName();
        Storage::disk('local')->putFileAs(
            'public/photos',$uploadFile,$filename
        );
        $photo=new Photo();
        $photo->original_name=$original_name;
        $photo->path=$filename;
        $photo->user_id=Auth::user()->id;
        $photo->save();
        return response()->json([
            'photo_id'=>$photo->id
        ]);
    }
    public function slideUpload(Request $request)
    {
        $uploadFile=$request->file('file');
        //$filename=time().$uploadFile->getClientOriginalName();
        $extension = $request->file('file')->extension();

        $filename=time().'slider-photo'.'.'.$extension;
        $original_name=$uploadFile->getClientOriginalName();
        Storage::disk('local')->putFileAs(
            'public/photos',$uploadFile,$filename
        );
        $slider=new Slider();
        $slider->original_name=$original_name;
        $slider->path=$filename;
        $slider->user_id=Auth::user()->id;
        $slider->save();
        return response()->json([
            'slider_id'=>$slider->id
        ]);
    }
    public function store(Request $request){
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}