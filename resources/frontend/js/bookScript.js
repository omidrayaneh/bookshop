$(document).ready(function(){

    /////////////////////////////////////////////////
    // ............ Carousel-gallery2 ............. //
    /////////////////////////////////////////////////
    let bookgallery=$('.book-gallery');
    if (bookgallery.length && $.fn.owlCarousel) {
        bookgallery.owlCarousel({
            rtl: true,
            nav: false,
            items: 3,
            dots: true,
            autoplay: true,
            loop: true,
            center: true,
            responsive: {
                0:{
                    items: 2
                },
                768:{
                    items: 5
                }
            }
        })
    }
    $('.book-gallery .owl-stage').addClass('d-flex align-items-center')

})