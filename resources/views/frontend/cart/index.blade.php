@extends('layouts.master')
@section('title')
    سبد خرید
@endsection
@section('content')
    <header class="head-gradiant " >
        <div class="container">
            <h1 class="mt-5"></h1>
            <p class="tagline"></p>
            <div class="py-5">
            </div>
        </div>
    </header>
    <div class="container-fluid light-bg mb-2 text-center">
        <div class="row p-3 mt-5">
            <div class="col-12 text-center">
                <p></p>
            </div>

        </div>
    </div>

    <div class="container-fluid light-bg mb-2 text-center">
        <div class="row p-3 mt-5">
            <div class="col-12 text-center">
                <p></p>
            </div>

        </div>
    </div>
        @if(Cart::count()!=0)
            <div class="container light-bg mb-2 text-center">
                <div class="row p-3 mt-5">
                    <div class="col-12 text-center">
                        <h2 class="title">سبد خرید</h2>
                    </div>
                </div>
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <div>{{session('success')}}</div>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <div>{{session('error')}}</div>
                    </div>
                @endif
            </div>
        @else
            <div class="container light-bg mb-2">
                <div class="row p-3 mt-5">
                    <div class="col-12 text-center">
                        <h3 class="title">سبد خرید شما خالی است</h3>
                        <button onclick="location.href='{{url('/')}}'" type="button">بازکشت به فروشگاه</button>

                    </div>
                </div>
            </div>
        @endif
        <div class="container">
            <div class="row">
                @if(Cart::count()!=0)
                    <div class="col-md-12">
                        <div class="card p-2 mb-2">
                            <h3 class="d-flex justify-content-between align-items-center">
                                <span>سبد خرید شما</span>
                                <span class="badge badge-dark badge-pill">{{Cart::count()}}</span>
                            </h3>
                            <?php $total = 0;?>
                            <div class="list-group">
                                @foreach(Cart::content() as $row)
                                    <?php
                                    if ($row->options->free == 0) {
                                        $total += $row->price;
                                    } else {
                                        $total += $row->price;
                                    }
                                    ?>
                                    <li class="list-group-item d-flex justify-content-between">
                                        <div>
                                            <h5 class="my-0">{{$row->name}}</h5>
                                        </div>
                                        <span class="text-muted">{{$row->price}} تومان</span>

                                    </li>
                                @endforeach
                                <li class="list-group-item d-flex justify-content-between">
                                    <div>
                                        <h5 class="my-0">جمع کل (تومان)</h5>
                                    </div>
                                    <span class="text-muted">{{$total}} تومان</span>
                                </li>
                            </div>

                            <input type="submit" class="btn btn-primary mb-6 btn-lg btn-block"
                                   onclick="location.href='{{ route('order.verify') }}'" value="پرداخت نهایی">
                        </div>
                    </div>
            </div>
        </div>
    @endif

@endsection
