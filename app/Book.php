<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function categories()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function photos()
    {
        return $this->belongsToMany(Photo::class);
    }
    public function voices()
    {
        return $this->belongsToMany(Voice::class);
    }
    public function authors()
    {
        return $this->belongsTo(Author::class,'author_id');
    }
    public function publishers()
    {
        return $this->belongsTo(Publisher::class,'publisher_id');
    }
    public function narrators()
    {
        return $this->belongsTo(Narrator::class,'narrator_id');
    }
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
