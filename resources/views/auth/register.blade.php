<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="/frontend/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="/frontend/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport' />
    <title> Masneed </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="/frontend/assets/fonts/font-awesome/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="/frontend/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/frontend/assets/css/now-ui-kit.css" rel="stylesheet" />
    <link href="/frontend/assets/css/plugins/owl.carousel.css" rel="stylesheet" />
    <link href="/frontend/assets/css/plugins/owl.theme.default.min.css" rel="stylesheet" />
    <link href="/frontend/assets/css/main.css" rel="stylesheet" />
</head>

<body>
<div class="wrapper default">
    <div class="container">
        <div class="row">
            <div class="main-content col-12 col-md-7 col-lg-5 mx-auto">
                <div class="account-box">
                    <a href="#" class="logo">
                        <img src="" alt="">
                    </a>
                    <div class="account-box-title">ثبت‌ نام در مسنيد</div>
                    <div class="message-light">اگر قبلا با ایمیل ثبت‌ نام کرده‌اید، نیاز به
                        ثبت‌ نام مجدد
                         ندارید
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                <div>{{session('success')}}</div>
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                <div>{{session('error')}}</div>
                            </div>
                        @endif
                    </div>
                    <div class="account-box-content">

                        <form class="form-account" method="POST" action="{{route('user.register')}}">
                            <div class="form-account-title">نام</div>
                            <div class="form-account-row">
                                <label class="input-label"><i class="now-ui-icons users_single-02"></i></label>
                                <input id="name" name="name" class="input-field" type="text"
                                       placeholder="نام خود را وارد نمایید">
                            </div>
                            <div class="form-account-title">نام خانوادگی</div>
                            <div class="form-account-row">
                                <label class="input-label"><i class="now-ui-icons users_single-02"></i></label>
                                <input id="last_name" name="last_name" class="input-field" type="text"
                                       placeholder="نام خانوادگی خود را وارد نمایید">
                            </div>
                            <div class="form-account-title">پست الکترونیک</div>
                            <div class="form-account-row">
                                <label class="input-label"><i class="now-ui-icons users_single-02"></i></label>
                                <input id="email" name="email" class="input-field" type="email"
                                       placeholder="پست الکترونیک خود را وارد نمایید">
                            </div>
                            <div class="form-account-title">کلمه عبور</div>
                            <div class="form-account-row">
                                <label class="input-label"><i
                                            class="now-ui-icons ui-1_lock-circle-open"></i></label>
                                <input id="password" name="password" class="input-field" type="password"
                                       placeholder="کلمه عبور خود را وارد نمایید">
                            </div>
                            <div class="form-account-agree">
                                <label class="checkbox-form checkbox-primary">
                                    <input type="checkbox" checked="checked">
                                    <span class="checkbox-check"></span>
                                </label>
                                <label for="agree">
                                    <a href="#" class="btn-link-border">حریم خصوصی</a> و <a href="#"
                                                                                            class="btn-link-border">شرایط و قوانین</a> استفاده از سرویس های سایت
                                    مسنيد  را مطالعه نموده و با کلیه موارد آن موافقم.</label>
                            </div>
                            <div class="form-account-row form-account-submit">
                                <div class="parent-btn">
                                        @csrf
                                    <button type="submit" class="dk-btn dk-btn-info">
                                        ثبت نام در مسنيد
                                        <i class="now-ui-icons users_circle-08"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="account-box-footer">
                        <span>قبلا در مسنيد  ثبت‌ نام کرده‌اید؟</span>
                        <a href="{{route('login')}}" class="btn-link-border">وارد شوید</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<!--   Core JS Files   -->
<script src="/frontend/assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="/frontend/assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="/frontend/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="/frontend/assets/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="/frontend/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="/frontend/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Share Library etc -->
<script src="/frontend/assets/js/plugins/jquery.sharrre.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="/frontend/assets/js/now-ui-kit.js" type="text/javascript"></script>
<!--  CountDown -->
<script src="/frontend/assets/js/plugins/countdown.min.js" type="text/javascript"></script>
<!--  Plugin for Sliders -->
<script src="/frontend/assets/js/plugins/owl.carousel.min.js" type="text/javascript"></script>
<!--  Jquery easing -->
<script src="/frontend/assets/js/plugins/jquery.easing.1.3.min.js" type="text/javascript"></script>
<!-- Main Js -->
<script src="/frontend/assets/js/main.js" type="text/javascript"></script>

</html>