@extends('layouts.master')
@section('title')
    پروفایل کاربری
@endsection
@section('content')

    <header class="head-gradiant " >
        <div class="container">
            <h1 class="mt-5"></h1>
            <p class="tagline"></p>
            <div class="py-5">
            </div>
        </div>
    </header>
    <div class="container-fluid light-bg mb-2 text-center">
        <div class="row p-3 mt-5">
            <div class="col-12 text-center">
                <p></p>
            </div>

        </div>
    </div>

    <div class="container-fluid light-bg mb-2 text-center">
        <div class="row p-3 mt-5">
            <div class="col-12 text-center">
                <p></p>
            </div>

        </div>
    </div>
    <div class="container light-bg mb-2">
        <div class="row p-3 mt-5">
            <div class="col-12 text-center">
                <h2 class="title">حساب کاربری</h2>
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <div>{{session('success')}}</div>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <div>{{session('error')}}</div>
                    </div>
                @endif
                @if(Session::has('credit'))
                    <div class="alert alert-info">
                        <div>{{session('credit')}}</div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row d-flex flex-column align-items-center">
            <div class="col-md-12">
                <nav>
                    <ul class="nav col-tabs-list">

                    </ul>
                </nav>
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                           role="tab" aria-controls="nav-home" aria-selected="true">مشخصات فردی</a>
                        <a class="nav-item nav-link" id="nav-payment-tab" data-toggle="tab" href="#nav-payment"
                           role="tab" aria-controls="nav-profile" aria-selected="false">سوابق پرداخت</a>
                        <a class="nav-item nav-link" id="nav-purchased-tab" data-toggle="tab" href="#nav-purchased"
                           role="tab" aria-controls="nav-profile" aria-selected="false">کتابخانه من</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="bg-light">
                            <form method="post" action="/profile-rename/{{Auth::user()->id}}">
                                @csrf
                                <input type="hidden" name="_method" value="PATCH">
                                <ul class="col-tabs-text p-0">
                                    <li class="bg-light selected" data-content="inbox">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-8 order-1 px-1">
                                                    <div class="card p-2 mb-2">
                                                        <hr>
                                                        <form class="needs-validation was-validated">
                                                            <div class="row form-group">
                                                                <div class="col-md-6">
                                                                    <label for="firstname">نام</label>
                                                                    <input type="text" class="form-control" name="name"
                                                                           placeholder="" value="{{Auth::user()->name}}"
                                                                           required>
                                                                    <div class="invalid-feedback">
                                                                        لطفا نام را وارد کنید
                                                                    </div>
                                                                    <div class="valid-feedback">
                                                                        نام وارد شد
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label for="lastname">نام خانوادگی</label>
                                                                    <input type="text" class="form-control"
                                                                           name="last_name"
                                                                           value="{{Auth::user()->last_name}}"
                                                                           placeholder=""
                                                                           required>
                                                                    <div class="invalid-feedback">
                                                                        لطفا نام خانوادگی را وارد کنید
                                                                    </div>
                                                                    <div class="valid-feedback">
                                                                        نام خانوادگی وارد شد
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <label for="email">ایمیل </label>
                                                                <input type="email" class="form-control" name="email"
                                                                       placeholder="you@example.info"
                                                                       value="{{Auth::user()->email}} " disabled>
                                                                <div class="valid-feedback">
                                                                    ایمیل با موفقیت وارد شد
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 order-2 p-0">
                                                    <div class="card p-2 mb-2">
                                                        <h3 class="d-flex justify-content-between align-items-center">
                                                            <span>تغییر رمز عبور</span>
                                                            <span class="ti-lock ti-1x"></span>
                                                        </h3>
                                                        {{-- <form method="post" action="/profile-rename/{{Auth::user()->id}}">
                                                             @csrf
                                                             <input type="hidden" name="_method" value="PATCH">--}}
                                                        <div class="row form-group">

                                                            <div class="col-md-10">
                                                                <label for="newpass">رمز جدید</label>
                                                                <input type="password" name="newpass"
                                                                       class="form-control"
                                                                       placeholder="">
                                                                <div class="invalid-feedback">
                                                                    لطفا رمز جدید را وارد کنید
                                                                </div>
                                                                <div class="valid-feedback">
                                                                    وارد شد
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <label for="renewpass">تکرار رمز جدید</label>
                                                                <input type="password" name="renewpass"
                                                                       class="form-control"
                                                                       placeholder="">
                                                                <div class="invalid-feedback">
                                                                    لطفا تکرار رمز جدید را وارد کنید
                                                                </div>
                                                                <div class="valid-feedback">
                                                                    وارد شد
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <input type="submit" class="btn btn-secondary " name=""
                                           value="ذخیره" style="float: left;">
                                </ul>

                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-payment" role="tabpanel" aria-labelledby="nav-payment-tab">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table ">
                                    <thead>
                                    <tr>
                                        <th class="text-center rx-banafsh">شناسه فاکتور</th>
                                        <th class="text-center rx-banafsh">شناسه پرداخت</th>
                                        <th class="text-center rx-banafsh">قیمت</th>
                                        <th class="text-center rx-banafsh">تاریخ سفارش</th>
                                        <th class="text-center rx-banafsh">وضعیت سفارش</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td class="text-center ">
                                                <a>{{$order->id}}</a></td>
                                            <td class="text-center ">
                                                <a>@if($order->RefID==null && $order->status==0)----@elseif($order->RefID==null && $order->status==1)   رایگان @else  {{$order->RefID}} @endif</a></td>
                                            <td class="text-center">{{$order->amount}}</td>
                                            @php $created_at = new Verta($order->created_at);@endphp
                                            <td class="text-center">{{$created_at}}</td>
                                            @if($order->status==1)
                                                <td class="text-center"><span style="color: #00fba5" >تسویه شده</span></td>
                                            @else
                                                <td class="text-center"><span style="color: #fb3449" >تسویه نشده</span></td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="col-md-6" >{{$orders->links()}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-purchased" role="tabpanel" aria-labelledby="nav-purchased-tab">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table ">
                                    <thead>
                                    <tr>
                                        <th class="text-center rx-banafsh">شناسه کتاب</th>
                                        <th class="text-center rx-banafsh">نام کتاب</th>
                                        <th class="text-center rx-banafsh">وضعیت کتاب</th>
                                        <th class="text-center rx-banafsh">قیمت</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($book_purchased as $item)
                                        <tr>
                                            <td class="text-center "><a href="{{route('book',['id'=>$item->id])}}">{{$item->sku}}</a></td>
                                            <td class="text-center "><a>{{$item->title}}</a></td>
                                            @if($item->amount==0)
                                                <td class="text-center"><span style="color: #00fba5" >رایگان</span></td>
                                            @else
                                                <td class="text-center"><span style="color: #fb3449" >خریداری شده</span></td>
                                            @endif
                                            <td class="text-center "><a>{{$item->price}}</a></td>
                                            <td>
                                                <button type="button" class="btn btn-danger" onclick="window.location='{{route('book',['id'=>$item->id])}}'">مطالعه کتاب</button>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="col-md-6" >{{$book_purchased->links()}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab"></div>
            </div>

        </div>
    </div>
    </div>


@endsection