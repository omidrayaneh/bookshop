<?php

namespace App\Http\Controllers\Admin;

use App\Bank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Validator;
class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banks=Bank::paginate(10);
        return view('admin.banks.index',compact(['banks']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banks=Bank::where('status',1)->get();
        foreach ($banks as $bank){
           $d= DB::table('banks')->where('id',$bank->id)->update(['status' => 0]);
        }

        $validator=Validator::make($request->all(),[
            'id'=>'required|unique:banks',
            'name'=>'required|unique:banks',
            'description'=>'required',
        ],[
            'id.required'=>'شناسه بانک  خالیست',
            'id.unique'=>'شناسه بانک قبلا ثبت شده است',
            'name.required'=>'عنوان بانک  خالیست',
            'name.unique'=>'این بانک قبلا ثبت شده است',
            'description.required'=>'توضیحات خالیست',
        ]);
        if($validator->fails()){
            return redirect('/admin/banks/create')->withErrors($validator)->withInput();
        }
        else {
            $newBank = new Bank();
            $newBank->name = $request->name;
            $newBank->merchantID = $request->id;
            $newBank->description = $request->description;
            $newBank->status = $request->status;
            $newBank->save();
            Session::flash('success', 'درگاه بانکی با موفقیت اضافه شد.');
            return redirect('/admin/banks');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank =Bank::findOrfail($id);
        return view('admin.banks.edit', compact(['bank']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banks=Bank::where('status',1)->get();
        foreach ($banks as $bank){
            $d= DB::table('banks')->where('id',$bank->id)->update(['status' => 0]);
        }
        $validator=Validator::make($request->all(),[
            'id'=>'required|unique:banks,merchantID,'.$id,
            'name'=>'required|unique:banks,name,'.$id,
            'description'=>'required',
        ],[
            'merchantID.required'=>'شناسه بانک  خالیست',
            'merchantID.unique'=>'شناسه بانک قبلا ثبت شده است',
            'name.required'=>'عنوان بانک  خالیست',
            'name.unique'=>'این بانک قبلا ثبت شده است',
            'description.required'=>'توضیحات خالیست',
        ]);
        if($validator->fails()){
            return redirect('/admin/banks/'.$id.'/edit')->withErrors($validator)->withInput();
        }
        else {
            $bank =Bank::findOrFail($id);
            $bank->name = $request->name;
            $bank->merchantID = $request->id;
            $bank->description = $request->description;
            $bank->status = $request->status;
            $bank->save();
            Session::flash('success', 'درگاه بانکی با موفقیت ویرایش شد.');
            return redirect('/admin/banks');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank = Bank::findOrFail($id);
        $bank->delete();

        Session::flash('error', 'درگاه بانکی با موفقیت حذف شد.');
        return redirect('/admin/banks');
    }
}
