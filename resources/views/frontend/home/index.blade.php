@extends('layouts.master')
@section('title')
    دانلود کتاب صوتی | masneed.com
@endsection
@section('content')
    <header id="home">
        <div class="head-gradiant"></div>
        <div class="headSlider">
            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($slider as $photo)
                        <div class="carousel-item @if($first_slide->id==$photo->id)active @endif">
                            <img class="d-block w-100" src="/storage/photos/{{$photo->path}}">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!--<div class="matn">
            <div class="titr px-3">
                <h1 class="mt-5"></h1>
                <p class="tagline"></p>
                <div class="py-5">
                </div>
            </div>
        </div>-->
    </header>

    {{--books--}}
    <div class="section light-bg py-4 book-list">
        <div class="container text-center">

            @if(Session::has('success'))
                <div class="alert alert-success">
                    <div>{{session('success')}}</div>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <div>{{session('error')}}</div>
                </div>
            @endif
               {{-- پرفروش ترین--}}
                    @if($ordered_books->count() && count($setting)!=0 && $setting->order==1)
                    <h3>پرفروش ترین ها</h3>
                    <div class="book-gallery owl-carousel time2">
                        @foreach($ordered_books as $book )
                                <a style="text-decoration:none" href="{{route('book',['id'=>$book->id])}}"><img
                                            src="/storage/photos/{{$book->path}}" class="img-fluid">
                                    <div class="text-center pt-2 h4"><p>{{$book->title}}</p>
                                        <p>{{$book->name}}</p></div>
                                </a>
                        @endforeach
                    </div>
                @endif
                {{-- پر بازدید ترین ها--}}
                @if($visited_books->count() && count($setting)!=0 && $setting->visit==1)
                    <h3> پر بازدید ترین ها</h3>
                    <div class="book-gallery owl-carousel time2">
                        @foreach($visited_books as $book)
                            <a style="text-decoration:none" href="{{route('book',['id'=>$book->id])}}"><img
                                        src="/storage/photos/{{$book->path}}" class="img-fluid">
                                <div class="text-center pt-2 h4"><p>{{$book->title}}</p>
                                    <p>{{$book->name}}</p></div>
                            </a>
                        @endforeach
                    </div>
                @endif
                {{-- آخرین کتاب ها--}}
                @if($lasted_add_books->count()&& count($setting)!=0 && $setting->last==1)
                    <h3> آخرین کتاب ها</h3>
                    <div class="book-gallery owl-carousel time2">
                        @foreach($lasted_add_books as $book)
                            <a style="text-decoration:none" href="{{route('book',['id'=>$book->id])}}"><img
                                        src="/storage/photos/{{$book->path}}" class="img-fluid">
                                <div class="text-center pt-2 h4"><p>{{$book->title}}</p>
                                    <p>{{$book->name}}</p></div>
                            </a>
                        @endforeach
                    </div>
                @endif
                {{--گروه ها--}}
            @foreach($all_category as $category)
                    @foreach($category_books as $book)
                        <h3  @if($book->category_id!=$category->id) style="display: none" @endif>{{$category->title}}</h3>
                        <div class="book-gallery owl-carousel time2" @if($book->category_id!=$category->id) style="display: none" @endif>
                        @if($category->id == $book->category_id )
                            <a style="text-decoration:none" href="{{route('book',['id'=>$book->id])}}"><img
                                        src="/storage/photos/{{$book->path}}" class="img-fluid">
                                <div class="text-center pt-2 h4"><p>{{$book->title}}</p>
                                    <p>{{$book->name}}</p></div>
                            </a>
                        @endif
                    @endforeach
                </div>
            @endforeach
            <div class="section-title mb-1 ">
            </div>
        </div>
    </div>

    {{--Comments --}}
    <div class="section light-bg pt-5" id="personal" @if(!$comments->count()) style="display: none" @endif>
        <div class="container">
            <div class="section-title">
                <h3>آنچه مشتریان ما می گویند</h3>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4  mb-0 mb-md-2">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <img class="profile pl-2" src="/frontend/images/profile2.png" alt="">
                                <div class="media-body">
                                    @foreach($comments as $comment)
                                        <div class="card-title"><h4>{{$comment->name}}</h4></div>
                                        <div class="card-text">{{$comment->description}}</div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--Subscription(planes)--}}
    <div class="section pt-5" id="panels">
        <div class="container">
            <div class="section-title">
                <h3>ارتقاء به اکانت ویژه</h3>
            </div>
            <div class="card-deck">
                @foreach($subscriptions as $subscription)
                    <div class="card panels">
                        <div class="card-header">
                            <h3>{{$subscription->title}}</h3>
                        </div>
                        <ul class="info-panel">
                            <li class="infos">   {{$subscription->limited_book}} کتاب رایگان</li>
                        </ul>
                        <form method="post" action="{{ route('credit.verify', $subscription) }}" accept-charset="UTF-8">
                            @csrf
                            <input type="hidden" name="_method" value="PATCH">
                            <div class="card-body">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">{{$subscription->price}}
                                    تومان
                                </button>
                            </div>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="section bg-gradiant download">
        <div class="container">
            <div class="section-title">
                <div class="box-icon"><span class="ti-announcement ti-3x gradiant-icon"></span></div>
                <h3 class="download-title">همکاری با ما</h3>
                <a href="#">
                    <button class="btn btn-light">اطلاعات بیشتر<span><img class="icon-store"
                                                                          src="/frontend/images/Handshake.png"
                                                                          alt=""></span></button>
                </a>
                <p>
                    <small><i>با همکاری در فروش کسب درآمد کنید</i></small>
                </p>
            </div>
        </div>
    </div>
@endsection