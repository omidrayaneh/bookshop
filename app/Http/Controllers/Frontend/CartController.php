<?php

namespace App\Http\Controllers\Frontend;

use App\Book;
use App\Vip;
use App\Category;
use App\Order;
use App\Order_Item;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{


    public function index()
    {
        $categories = Category::all();
        $count = count($categories);
        $categories1 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset(0)
            ->limit($count / 2)->get();
        $categories2 = Category::with('childrenRecursive')
            ->where('parent_id', null)->offset($count / 2)
            ->limit($count / 2)->get();


        return view('frontend.cart.index', compact(['categories1', 'categories2', 'books', 'comments', 'subscriptions']));
    }

    public function cart()
    {
        $cart = Cart::Content();
        return view('cart', compact('cart'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request)
    {
        foreach (Cart::Content() as $cart) {
            if ($cart->id == $request->id) {
                return back();
            }
        }
        $book = Book::with('photos')->whereId($request->id)->first();
        $free= $book->free;
        $orders=Order::with('order_items')->where([
            ['user_id',Auth::user()->id],
            ['status',1]])->get();
        foreach ($orders as $order){
            $order_item=Order_Item::where([['order_id',$order->id],['book_id',$book->id]])->first();
            if ($order_item){
                Session::flash('error',' کتاب " ' . $book->title  .' " را قبلا خریداری کرده اید ');
                return redirect('/');
            }
        }
        $price = $book->price;
        $discount_percent = ($book->discount_percent);
        $discount = (($price*$discount_percent)/100);
        $book_total_price = $price - $discount;
        $path = $book->photos[0]->path;


        //check user has vip
        $vip=Vip::where([['user_id',Auth::user()->id],['status','>', 0]])->whereColumn('start_day','<', 'end_day')->first();


        // book is free
        if($free==1){
            $added = Cart::add([
                'id' => $book->id,
                'name' => $book->title,
                'qty' => '1',
                'price' => 0,
                'discount' => 0,
                'weight' => '',
                'discountRate'=>0,
                'options' => [
                    'path' => $path,
                    'discount' => 0,
                    'free'=> 1,

                ],
            ]);

            if ($added) {
                foreach (Cart::Content() as $new_cart){
                    if ($new_cart->name==$book->title){
                        DB::table('cart_models')->insert([
                            ['id'=>$book->id,'book_id'=>$new_cart->rowId,'name' => $book->title,'price' => 0,'discount' => 0,'path' => $path,'free' => 1]
                        ]);
                    }
                }
                return redirect('/cart');
            }
        }
        if ($vip){
            if( $vip->status>0){
                //credit more than book price show message and status>0
                $added = Cart::add([
                    'id' => $book->id,
                    'name' => $book->title,
                    'qty' => '1',
                    'price' => 0,
                    'discount' => 0,
                    'weight' => '',
                    'discountRate'=>$discount_percent,
                    'options' => [
                        'path' => $path,
                        'discount' => 0,
                        'free'=> 1,

                    ],
                ]);

                DB::table('vips')->where('user_id',Auth::user()->id)->update(['status' => $vip->status-1]);

                if ($added) {
                    foreach (Cart::Content() as $new_cart){
                        if ($new_cart->name==$book->title){
                            DB::table('cart_models')->insert([
                                ['id'=>$book->id,'book_id'=>$new_cart->rowId,'name' => $book->title,'price' => 0,'discount' => 0,'path' => $path,'free' => 0]
                            ]);
                        }
                    }
                    return redirect('/profile');
                }
            }else{
                //book more than credit price  show message and status=0
                $added = Cart::add([
                    'id' => $book->id,
                    'name' => $book->title,
                    'qty' => '1',
                    'price' => $book_total_price,
                    'discount' => $discount,
                    'weight' => '',
                    'discountRate'=>$discount_percent,
                    'options' => [
                        'path' => $path,
                        'discount' => $discount,
                        'free'=> 0,
                    ],
                ]);
                if ($added) {
                    foreach (Cart::Content() as $new_cart){
                        if ($new_cart->name==$book->title){
                            DB::table('cart_models')->insert([
                                ['id'=>$book->id,'book_id'=>$new_cart->rowId,'name' => $book->title,'price' => $book_total_price,'discount' => $discount,'path' => $path,'free' => 0]
                            ]);
                        }
                    }

                    Session::flash('error','اعتبار شما برای خرید رایگان این کتاب کافی نیست. برای دریافت رایگان این کتاب اکانت خود را شارژ نمایید');
                    return redirect('/cart');
                }
                return back();
            }
        }
        else{
            //no eny credit
            $added = Cart::add([
                'id' => $book->id,
                'name' => $book->title,
                'qty' => '1',
                'price' => $book_total_price,
                'discount' => $discount,
                'weight' => '',
                'discountRate'=>$discount_percent,
                'options' => [
                    'path' => $path,
                    'discount' => $discount,
                    'free'=> 0,
                ],
            ]);

            if ($added) {

                foreach (Cart::Content() as $new_cart){
                    if ($new_cart->name==$book->title){
                        DB::table('cart_models')->insert([
                            ['id'=>$book->id,'book_id'=>$new_cart->rowId,'name' => $book->title,'price' => $book_total_price,'discount' => $discount,'path' => $path,'free' => 0]
                        ]);
                    }
                }
                Session::flash('error','برای دریافت رایگان کتاب اکانت خود را به اکانت ویژه ارتقا دهید');
                return redirect('/cart');
            }
        }

    }
    public function removeItem(Request $request,$id){

        $vip=Vip::where('user_id',Auth::user()->id)->first();
        foreach (Cart::Content() as $item){

            $book = Book::whereId($item->id)->first();

          if ($item->rowId==$id && $item->price==0){
              if ($book->free=0)
              DB::table('vips')->where('user_id',Auth::user()->id)->update(['status' => $vip->status+1]);
              $b=  DB::table('cart_models')->where('id',$item->id)->delete();
              Cart::remove($id);
              return redirect('/cart');
          }
          elseif ($item->rowId==$id && $item->price!=0){

             $b=  DB::table('cart_models')->where('book_id',$id)->delete();
              Cart::remove($id);
              return back();
          }

       }


    }


}
