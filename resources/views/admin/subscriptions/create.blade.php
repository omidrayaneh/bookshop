@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>دسته بندی ها</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right">ایجاد دسته بندی جدید</h3>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form method="post" action="/admin/subscriptions">
                                @csrf
                                <div class="form-group">
                                    <label for="title">عنوان اشتراک</label>
                                    <input type="text" name="title" class="form-control" placeholder="عنوان اشتراک ...">
                                </div>
                                <div class="form-group">
                                    <label for="price">قیمت</label>
                                    <input type="number" name="price" class="form-control" placeholder="قیمت اشتراک ...">
                                </div>
                                {{--<div class="form-group">
                                    <label for="limited_price">اعتبار مصرف</label>
                                    <input type="number" name="limited_price" class="form-control" placeholder="اعتبار اشتراک ...">
                                </div>--}}
                                <div class="form-group">
                                    <label for="limited_book">تعداد کتاب</label>
                                    <input type="number" name="limited_book" class="form-control" placeholder="تعداد کتاب رایگان برای اشتراک ...">
                                </div>
                                <div class="form-group">
                                    <label for="period">مدت زمان (ماه)</label>
                                    <input type="number" name="period" class="form-control" placeholder="مدت زمان اشتراک ...">
                                </div>
                                <div class="form-group">
                                    <label>وضعیت</label>
                                    <div>
                                        <input type="radio" name="status" value="0" checked> <span class="margin-l-10">فعال</span>
                                        <input type="radio" name="status" value="1"> <span>غیر فعال</span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection