@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>نظر ها</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right"> نظر ها </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            <div>{{session('success')}}</div>
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <div>{{session('error')}}</div>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th class="text-right">ردیف</th>
                                <th class="text-center">شناسه</th>
                                <th class="text-right">عنوان کتاب</th>
                                <th class="text-right">متن نظر</th>
                                <th class="text-right">وضعیت</th>
                                <th class="text-right">عملیات</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php $i=1 @endphp
                            @foreach($comments as $comment)
                                <tr>
                                    <td class="text-right">{{$i}}</td>
                                    <td class="text-center">{{$comment->id}}</td>
                                    <td class="text-right">{{$comment->book->title}}</td>
                                    <td class="text-right">{{$comment->description}}</td>
                                    @if($comment->status==1)
                                        <td><span class="label label-success">منتشر شده</span></td>
                                    @else
                                        <td><span class="label label-danger">منتشر نشده</span></td>
                                    @endif
                                    @if($comment->status==1)
                                        <td>
                                            <form method="post" action="{{route('comments.actions',$comment->id)}}">
                                                @csrf
                                                <div class="from-group">
                                                    <input name="action" type="hidden" value="unapproved">
                                                    <input type="submit" class="btn btn-danger pull-right" value="عدم تایید">
                                                </div>
                                            </form>
                                        </td>
                                    @else
                                        <td>
                                            <form method="post" action="{{route('comments.actions',$comment->id)}}">
                                                @csrf
                                                <div class="from-group">
                                                    <input name="action" type="hidden" value="approved">
                                                    <input type="submit" class="btn btn-success pull-right" value="تایید">
                                                </div>
                                            </form>
                                        </td>
                                    @endif


                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                        <div class="col-md-12" style="text-align: center">{{$comments->links()}}</div>

                    </div>
                    <!-- /.table-responsive -->
                </div>
            </div>
        </section>

    </div>
@endsection