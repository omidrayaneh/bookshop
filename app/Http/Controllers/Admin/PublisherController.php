<?php

namespace App\Http\Controllers\Admin;

use App\Publisher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;

class PublisherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publishers=Publisher::paginate(10);
        return view('admin.publishers.index',compact('publishers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.publishers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'required|unique:publishers',
        ],[
            'name.required'=>'عنوان ناشر  خالیست',
            'name.unique'=>'این ناشر قبلا ثبت شده است',
        ]);
        if($validator->fails()){
            return redirect('/admin/publishers/create')->withErrors($validator)->withInput();
        }
        else {
            $publishers = new Publisher();
            $publishers->name = $request->name;
            $publishers->status = $request->status;
            $publishers->save();
            Session::flash('success', 'ناشر با موفقیت اضافه شد.');
            return redirect('/admin/publishers');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publisher = Publisher::findOrfail($id);
        return view('admin.publishers.edit', compact(['publisher']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'required|unique:publishers,name,'.$id,
        ],[
            'name.required'=>'عنوان ناشر  خالیست',
            'name.unique'=>'این ناشر قبلا ثبت شده است',
        ]);
        if($validator->fails()){
            return redirect('/admin/publishers/create')->withErrors($validator)->withInput();
        }
        else {
            $publisher = Publisher::findOrFail($id);
            $publisher->name = $request->name;
            $publisher->status = $request->status;
            $publisher->save();
            Session::flash('success', 'ناشر با موفقیت ویرایش شد.');
            return redirect('/admin/publishers');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $publisher = Publisher::findOrFail($id);
        $publisher->delete();

        Session::flash('error', 'ناشر با موفقیت حذف شد.');
        return redirect('/admin/publishers');
    }
}
