@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>کاربر ها</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right"> کاربر ها </h3>
                    <div class=" text-left">
                        <a class="btn btn-app" href="{{route('users.create')}}">
                            <i class="fa fa-plus"></i> جدید
                        </a>
                    </div>
                </div>

                <div class="box-header with-border">
                    <h3 class="box-title pull-right">لیست کاربر ها </h3>
                    <div class=" text-left">

                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            <div>{{session('success')}}</div>
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <div>{{session('error')}}</div>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th class="text-right">ردیف</th>
                                <th class="text-center">شناسه</th>
                                <th class="text-right">نام کاربری</th>
                                <th class="text-right">ایمیل</th>
                                <th class="text-right"></th>

                            </tr>
                            </thead>
                            <tbody>
                            @php $i=1 @endphp
                            @foreach($users as $user)
                                <tr>
                                    <td class="text-right">{{$i}}</td>
                                    <td class="text-center">{{$user->id}}</td>
                                    <td class="text-right">{{$user->name}}</td>
                                    <td class="text-right">{{$user->email}}</td>

                                    <td class="text-center">
                                        <div class="display-inline-block">
                                            <form method="post" action="/admin/users/{{$user->id}}">
                                                <a href="{{route('users.edit',$user->id)}}" class="btn btn-warning" >ویرایش</a>
                                                @csrf
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit"  class="btn btn-danger " >حذف</button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @php $i++; @endphp
                            @endforeach
                            </tbody>
                        </table>
                        <div class="col-md-12" style="text-align: center">{{$users->links()}}</div>

                    </div>
                    <!-- /.table-responsive -->
                </div>
            </div>
        </section>

    </div>
@endsection