@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>ناشر ها</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right">ایجاد ناشر جدید</h3>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form method="post" action="/admin/publishers">
                                @csrf
                                <div class="form-group">
                                    <label for="name">عنوان ناشر</label>
                                    <input type="text" name="name" class="form-control" placeholder="عنوان اشتراک ...">
                                </div>
                                <div class="form-group">
                                    <label>وضعیت</label>
                                    <div>
                                        <input type="radio" name="status" value="1" checked> <span class="margin-l-10">فعال</span>
                                        <input type="radio" name="status" value="0"> <span>غیر فعال</span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection