@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>اسلاید ها</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right"> لیست اسلایدها </h3>
                    <div class=" text-left">
                        <a class="btn btn-app" href="{{route('sliders.create')}}">
                            <i class="fa fa-plus"></i> جدید
                        </a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            <div>{{session('success')}}</div>
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <div>{{session('error')}}</div>
                        </div>
                    @endif

                        <div>
                            <label>ترتیب نمایش اسلاید ها از کوچک به بزرگ</label>
                        </div>
                        @php $i=1 @endphp
                        @foreach($sliders as $photo)
                            <div class="remove-img col-sm-3" id="updated_photo_{{$photo->id}}">
                                <label>اسلاید شماره {{ $i }}</label>
                                <img class="img-responsive" src="/storage/photos/{{$photo->path}}">
                                <form method="post" action="/admin/sliders/{{$photo->id}}">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="btn btn-danger ">حذف</button>
                                </form>
                            </div>
                            @php $i++; @endphp
                        @endforeach
                </div>
            </div>
        </section>

    </div>
@endsection

