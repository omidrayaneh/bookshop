@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>دسته بندی ها</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right"> دسته بندی ها </h3>
                    <div class=" text-left">
                        <a class="btn btn-app" href="{{route('categories.create')}}">
                            <i class="fa fa-plus"></i> جدید
                        </a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            <div>{{session('success')}}</div>
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <div>{{session('error')}}</div>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th class="text-center">شناسه</th>
                                <th class="text-right">عنوان</th>
                                <th class="text-right">انتشار در صفحه اول</th>
                                <th class="text-center"></th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td class="text-center">{{$category->id}}</td>
                                    <td class="text-right">{{$category->title}}</td>
                                    @if($category->first_page==1)
                                        <td><span class="label label-success"> نمایش داده شود</span></td>
                                    @else
                                        <td><span class="label label-danger">نمایش داده نشود</span></td>
                                    @endif
                                    <td class="text-center">
                                        <div class="display-inline-block">
                                            <form method="post" action="/admin/categories/{{$category->id}}">
                                                <a href="{{route('categories.edit',$category->id,compact($categories))}}" class="btn btn-warning" >ویرایش</a>
                                                @csrf
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit"  class="btn btn-danger " >حذف</button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @if(count($category->childrenRecursive)>0)
                                    @include('admin.partials.category_list',['categories'=>$category->childrenRecursive,'level'=>1])
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        <div class="col-md-12" style="text-align: center">{{$categories->links()}}</div>
                    </div>
                    <!-- /.table-responsive -->
                </div>
            </div>
        </section>

    </div>
@endsection