$(document).ready(function(){

    /////////////////////////////////////////////////
    // .............. color-box .............. //
    /////////////////////////////////////////////////
    $('.inline').colorbox({
        inline:true,
        width:"450",
    });

    /////////////////////////////////////////////////
    // .............. scroll-top .............. //
    /////////////////////////////////////////////////
    function menuscroll(){
        let navmenu= $('.nav-menu');
        if ($(window).scrollTop() > 50) {
            navmenu.addClass('is-scrolling')
        }else{
            navmenu.removeClass('is-scrolling')
        }
    }

    /////////////////////////////////////////////////
    // .............. collapse-show .............. //
    /////////////////////////////////////////////////
    menuscroll();
    $(window).on('scroll',menuscroll)

    let navslide= $('#navbar');
    // let navs=$('#navbar ul.navbar-nav li.nav-item');

    navslide.on('show.bs.collapse',function(){
        $(this).parents('.nav-menu').addClass('nav-is-open');
        // navs.children('a').removeClass('active');
    })
    navslide.on('hide.bs.collapse',function(){
        $(this).parents('.nav-menu').removeClass('nav-is-open');
    })

    /////////////////////////////////////////////////
    // ............ Carousel-gallery ............. //
    /////////////////////////////////////////////////
    let imggallery=$('.img-gallery');
    if (imggallery.length && $.fn.owlCarousel) {
        imggallery.owlCarousel({
            rtl: true,
            nav: false,
            items: 3,
            dots: true,
            autoplay: true,
            loop: true,
            center: true,
            responsive: {
                0:{
                    items: 2
                },
                768:{
                    items: 3
                }
            }
        })
    }
    $('.img-gallery .owl-stage').addClass('d-flex align-items-center')

    /////////////////////////////////////////////////
    // ............ Carousel-gallery2 ............. //
    /////////////////////////////////////////////////
    let bookgallery=$('.book-gallery');
    if (bookgallery.length && $.fn.owlCarousel) {
        bookgallery.owlCarousel({
            rtl: true,
            nav: false,
            items: 3,
            dots: true,
            autoplay: true,
            loop: true,
            center: true,
            responsive: {
                0:{
                    items: 2
                },
                768:{
                    items: 5
                }
            }
        })
    }
    $('.book-gallery .owl-stage').addClass('d-flex align-items-center')

})