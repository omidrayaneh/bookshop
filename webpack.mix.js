const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');*/
mix.js('resources/js/app.js', 'public/admins/js');
mix.styles(['resources/admin/css/dropzone.min.css'],'public/admins/dist/css/dropzone.css')
    .scripts(['resources/admin/js/dropzone.min.js'],'public/admins/dist/js/dropzone.js')
