<?php

namespace App\Http\Controllers\Admin;

use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptions =Subscription::paginate(10);
        return view('admin.subscriptions.index', compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.subscriptions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'title'=>'required|unique:subscriptions',
            'price' => 'required',
            'period'=>'required',
           // 'limited_price'=>'required',
            'limited_book'=>'required',

        ],[
            'title.required'=>'عنوان اشتراک  خالیست',
            'title.unique'=>'این اشتراک قبلا ثبت شده است',
            'price.required'=>'مبلغ اشتراک خالیست',
            'period.required'=>'مدت زمان اشتراک خالیست',
           // 'limited_price.required'=>'مقدار اعتبار اشتراک خالیست',
            'limited_book.required'=>'تعداد کتاب اشتراک خالیست',
        ]);
        if($validator->fails()){
            return redirect('/admin/subscriptions/create')->withErrors($validator)->withInput();
        }
        else {
            $newSubscription = new Subscription();
            $newSubscription->title = $request->title;
            $newSubscription->price = $request->price;
            $newSubscription->status = $request->status;
            $newSubscription->period = $request->period;
            //$newSubscription->limited_price = $request->limited_price;
            $newSubscription->limited_book = $request->limited_book;
            $newSubscription->user_id = Auth::user()->id;
            $newSubscription->save();
            Session::flash('success', 'اشتراک با موفقیت اضافه شد.');
            return redirect('/admin/subscriptions');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscription = Subscription::findOrfail($id);
        return view('admin.subscriptions.edit', compact(['subscription']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'title'=>'required|unique:subscriptions,title,'.$id,
            'price' => 'required',
            'period'=>'required',
           // 'limited_price'=>'required',
            'limited_book'=>'required',
        ],[
            'title.required'=>'عنوان اشتراک  خالیست',
            'title.unique'=>'این اشتراک قبلا ثبت شده است',
            'price.required'=>'مبلغ اشتراک خالیست',
            'period.required'=>'مدت زمان اشتراک خالیست',
           // 'limited_price.required'=>'مقدار اعتبار اشتراک خالیست',
            'limited_book.required'=>'تعداد کتاب اشتراک خالیست',
        ]);
        if($validator->fails()){
            return redirect('/admin/subscriptions/create')->withErrors($validator)->withInput();
        }
        else {
            $subscription = Subscription::findOrFail($id);
            $subscription->title = $request->title;
            $subscription->price = $request->price;
            $subscription->status = $request->status;
            $subscription->period = $request->period;
          //  $subscription->limited_price = $request->limited_price;
            $subscription->limited_book = $request->limited_book;
            $subscription->user_id = Auth::user()->id;
            $subscription->save();
            Session::flash('success', 'اشتراک با موفقیت ویرایش شد.');
            return redirect('/admin/subscriptions');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subscription = Subscription::findOrFail($id);
        $subscription->delete();

        Session::flash('error', 'اشتراک با موفقیت حذف شد.');
        return redirect('/admin/subscriptions');
    }
}
