<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class bookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'book',
            'last_name' => 'shop',
            'email' => 'bookshop@gmail.com',
            'password' => Hash::make('123456789'),
            'is_admin' => 1,
            'status' => 1,
        ]);

        DB::table('categories')->insert([
             ['title' => 'شعر','sku' => '45345','parent_id'=>null,'user_id'=>1]
            ,['title' => 'تاریخ، فلسفه','sku' => '13554','parent_id'=>null,'user_id'=>1]
            ,['title' => 'داستان کوتاه', 'sku' => '53453','parent_id'=>null,'user_id'=>1]
            ,['title' => 'داستان و رمان ایرانی', 'sku' => '25411','parent_id'=>null,'user_id'=>1]
            ,['title' => 'روانشناسی', 'sku' => '85545','parent_id'=>null,'user_id'=>1]
            ,['title' => 'زندگینامه', 'sku' => '02542','parent_id'=>null,'user_id'=>1]
            ,['title' => 'سخنرانی', 'sku' => '12458','parent_id'=>null,'user_id'=>1]
            ,['title' => 'شعر و ادبیات کهن', 'sku' => '62532','parent_id'=>null,'user_id'=>1]
            ,['title' => 'کودک و نوجوان', 'sku' => '85471','parent_id'=>null,'user_id'=>1]
            ,['title' => 'مدیریت و موفقیت', 'sku' => '85265','parent_id'=>null,'user_id'=>1]
            ,['title' => 'فرهنگ زندگی', 'sku' => '75245','parent_id'=>null,'user_id'=>1]]
        );
        DB::table('categories')->insert([
            ['title' => 'زیر گروه 1','sku' => '13554','parent_id'=>1,'user_id'=>1,'first_page'=>1],
            ['title' => 'زیر گروه 2','sku' => '13554','parent_id'=>2,'user_id'=>1,'first_page'=>0],
            ['title' => 'زیر گروه 3','sku' => '13554','parent_id'=>3,'user_id'=>1,'first_page'=>1],
            ['title' => 'زیر گروه 4','sku' => '13554','parent_id'=>4,'user_id'=>1,'first_page'=>0],
            ['title' => 'زیر گروه 5','sku' => '13554','parent_id'=>5,'user_id'=>1,'first_page'=>0],
            ['title' => 'زیر گروه 5','sku' => '13554','parent_id'=>6,'user_id'=>1,'first_page'=>0],
            ['title' => 'زیر گروه 6','sku' => '13554','parent_id'=>7,'user_id'=>1,'first_page'=>1],
            ['title' => 'زیر گروه 7','sku' => '13554','parent_id'=>8,'user_id'=>1,'first_page'=>0],
            ['title' => 'زیر گروه 8','sku' => '13554','parent_id'=>9,'user_id'=>1,'first_page'=>0],
            ['title' => 'زیر گروه 9','sku' => '13554','parent_id'=>10,'user_id'=>1,'first_page'=>0],
            ['title' => 'زیر گروه 10','sku' => '13554','parent_id'=>11,'user_id'=>1,'first_page'=>0],
        ]);

        DB::table('authors')->insert([
            ['name' => 'آرش بنیادی','status' => '1'],
            ['name' => 'رقیه زاکری','status' => '1'],
            ['name' => 'مریم میرزاخانی','status' => '1'],
            ['name' => 'نسرین ستوده','status' => '1'],
            ['name' => 'هادی بهادری','status' => '1'],
            ]);
        DB::table('publishers')->insert([
            ['name' => 'امیر کبیر','status' => '1'],
            ['name' => 'ققنوس','status' => '1'],
            ['name' => 'مریم میرزاخانی','status' => '1'],
            ['name' => 'گیوا','status' => '1'],
            ['name' => 'چشمه','status' => '1'],
        ]);
        DB::table('narrators')->insert([
            ['name' => 'رضا جلیل زاده','status' => '1'],
            ['name' => 'جواد والی زاده','status' => '1'],
            ['name' => 'رسول احمد زاده','status' => '1'],
            ['name' => 'خسرو شکیبایی','status' => '1'],
            ['name' => 'رضا عرفانی','status' => '1'],
        ]);
        DB::table('banks')->insert([
            ['name' => 'زرین پال','merchantID' => 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX','description' => 'توضیحات تراکنش تستی','status'=>1]
        ]);
        DB::table('subscriptions')->insert([
            ['title' => 'یک ماهه','price' => '10000','limited_book' => '2','period' => '1','status' => '1','user_id' => '1'],
            ['title' => 'سه ماهه','price' => '18000','limited_book' => '5','period' => '3','status' => '1','user_id' => '1'],
            ['title' => 'شش ماهه','price' => '36000','limited_book' => '6','period' => '6','status' => '1','user_id' => '1'],
            ['title' => 'دوازده ماهه','price' => '55000','limited_book' => '15','period' => '12','status' => '1','user_id' => '1']
        ]);
        DB::table('photos')->insert([
            ['path' => '1560751585pic (10).jpg','original_name' => 'pic (10).jpg','user_id' => '1'],
            ['path' => '1560751748pic (10).jpg','original_name' => 'pic (10).jpg','user_id' => '1'],
            ['path' => '1560751835pic (10).jpg','original_name' => 'pic (10).jpg','user_id' => '1'],
            ['path' => '1560752157pic (9).jpg','original_name' => 'pic (9).jpg','user_id' => '1'],
            ['path' => '1560752681pic4.jpg','original_name' => 'pic4.jpg','user_id' => '1'],
            ['path' => '1560752709pic4.jpg','original_name' => 'pic4','user_id' => '1'],
            ['path' => '1560752814pic5.jpg','original_name' => 'pic5.jpg','user_id' => '1'],
            ['path' => '1560752883pic6.jpg','original_name' => 'pic6.jpg','user_id' => '1'],
            ['path' => '1560752971pic2.jpg','original_name' => 'pic2.jpg','user_id' => '1'],
            ['path' => '1560753026pic.jpg','original_name' => 'pic.jpg','user_id' => '1'],
            ['path' => '1560753066pic (1).jpg','original_name' => 'pic (1).jpg','user_id' => '1'],
            ['path' => '1560753233pic (11).jpg','original_name' => 'pic (11).jpg','user_id' => '1'],
            ['path' => '1560753287pic (13).jpg','original_name' => 'pic (13).jpg','user_id' => '1'],
            ['path' => '1560753390pic7.jpg','original_name' => 'pic7.jpg','user_id' => '1'],
            ['path' => '1560753447pic (7).jpg','original_name' => 'pic (7).jpg','user_id' => '1'],
            ['path' => '1560753495pic (4).jpg','original_name' => 'pic (4).jpg','user_id' => '1'],
            ['path' => '1560753543pic (6).jpg','original_name' => 'pic (6).jpg','user_id' => '1'],
            ['path' => '1560753603pic (5).jpg','original_name' => 'pic (5).jpg','user_id' => '1'],
            ['path' => '1560753653pic (8).jpg','original_name' => 'pic (8).jpg','user_id' => '1'],
            ['path' => '1560753704pic (12).jpg','original_name' => 'pic (12).jpg','user_id' => '1'],
            ['path' => '1560753928pic3.jpg','original_name' => 'pic3.jpg','user_id' => '1'],
            ['path' => '1560753979pic (2).jpg','original_name' => 'pic (2).jpg','user_id' => '1'],
            ['path' => '1560754013pic (3).jpg','original_name' => 'pic (3).jpg','user_id' => '1'],

        ]);
        DB::table('voices')->insert([
            ['path' => '1560751589sample_18.ogv','original_name' => 'sample_18.ogv','user_id' => '1','free'=>1],
            ['path' => '1560751751sample_18.ogv','original_name' => 'sample_18.ogv','user_id' => '1','free'=>0],
            ['path' => '1560751838sample_18.ogv','original_name' => 'sample_18.ogv','user_id' => '1','free'=>0],
            ['path' => '560752162sample_17.ogv','original_name' => 'sample_17.ogv','user_id' => '1','free'=>0],
            ['path' => '1560752740sample_5.ogv','original_name' => 'sample_5.ogv','user_id' => '1','free'=>0],
            ['path' => '1560752762sample_9.ogv','original_name' => 'sample_9.ogv','user_id' => '1','free'=>0],
            ['path' => '1560752762sample_9.ogv','original_name' => 'sample_9.ogv','user_id' => '1','free'=>1],
            ['path' => '1560752817sample_9.ogv','original_name' => 'sample_9.ogv','user_id' => '1','free'=>1],
            ['path' => '1560752974sample_2.ogv','original_name' => 'sample_2.ogv','user_id' => '1','free'=>1],
            ['path' => '1560753029sample_3.ogv','original_name' => 'sample_3.ogv','user_id' => '1','free'=>0],
            ['path' => '1560753077sample_3.ogv','original_name' => 'sample_3.ogv','user_id' => '1','free'=>1],
            ['path' => '1560753239sample_19.ogv','original_name' => 'sample_19.ogv','user_id' => '1','free'=>0],
            ['path' => '1560753298sample_19.ogv','original_name' => 'sample_19.ogv','user_id' => '1','free'=>1],
            ['path' => '1560753394sample_11.ogv','original_name' => 'sample_11.ogv','user_id' => '1','free'=>0],
            ['path' => '1560753451sample_12.ogv','original_name' => 'sample_12.ogv','user_id' => '1','free'=>1],
            ['path' => '1560753497sample_13.ogv','original_name' => 'sample_13.ogv','user_id' => '1','free'=>0],
            ['path' => '1560753548sample_14.ogv','original_name' => 'sample_14.ogv','user_id' => '1','free'=>0],
            ['path' => '1560753607sample_15.ogv','original_name' => 'sample_15.ogv','user_id' => '1','free'=>0],
            ['path' => '1560753655sample_16.ogv','original_name' => 'sample_16.ogv','user_id' => '1','free'=>0],
            ['path' => '1560753706sample_20.ogv','original_name' => 'sample_20.ogv','user_id' => '1','free'=>0],
            ['path' => '1560753931sample_5.ogv','original_name' => 'sample_5.ogv','user_id' => '1','free'=>0],
            ['path' => '1560753981sample_6.ogv','original_name' => 'sample_6.ogv','user_id' => '1','free'=>0],
            ['path' => '1560754015sample_7.ogv','original_name' => 'sample_7.ogv','user_id' => '1','free'=>0],

        ]);

        DB::table('books')->insert([
            ['title' => 'کتاب صوتی نبرد','sku' => '20155','status'=>1,'free'=>0,'price'=>15000,'discount_percent'=>0,'user_id' => 1,'category_id'=>12,'author_id'=>4,'publisher_id'=>2,'narrator_id'=>3],
            [ 'title' =>'وینستون چرچیل','sku' => '23465','status'=> 1, 'free'=>0, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>12, 'author_id'=>1, 'publisher_id'=>3, 'narrator_id'=>1],
            [ 'title' =>'چهار اثر فلورانس اسکاول شین','sku' => '75163','status'=> 1, 'free'=>1, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>15, 'author_id'=>3, 'publisher_id'=>4, 'narrator_id'=>2],
            [ 'title' =>'از خوب به عالی','sku' => '14960','status'=> 1, 'free'=>0, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>18, 'author_id'=>3, 'publisher_id'=>3, 'narrator_id'=>3],
            ['title' => 'آرامش','sku' => '20155','status'=>1,'free'=>0,'price'=>15000,'discount_percent'=>0,'user_id' => 1,'category_id'=>12,'author_id'=>4,'publisher_id'=>2,'narrator_id'=>3],
            [ 'title' =>'دوست خوب من','sku' => '23465','status'=> 1, 'free'=>0, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>12, 'author_id'=>1, 'publisher_id'=>3, 'narrator_id'=>1],
            [ 'title' =>'دو اثر فلورانس اسکاول شین','sku' => '75163','status'=> 1, 'free'=>1, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>15, 'author_id'=>3, 'publisher_id'=>4, 'narrator_id'=>2],
            [ 'title' =>'یک روز خوب','sku' => '14960','status'=> 1, 'free'=>0, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>18, 'author_id'=>3, 'publisher_id'=>3, 'narrator_id'=>3],
            ['title' => 'شاهنامه','sku' => '20155','status'=>1,'free'=>0,'price'=>15000,'discount_percent'=>0,'user_id' => 1,'category_id'=>12,'author_id'=>4,'publisher_id'=>2,'narrator_id'=>3],
            [ 'title' =>'دیوان شمس تبریزی','sku' => '23465','status'=> 1, 'free'=>0, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>12, 'author_id'=>1, 'publisher_id'=>3, 'narrator_id'=>1],
            [ 'title' =>'بیشعوری','sku' => '75163','status'=> 1, 'free'=>1, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>14, 'author_id'=>3, 'publisher_id'=>4, 'narrator_id'=>2],
            [ 'title' =>'قصه کودکان','sku' => '14960','status'=> 1, 'free'=>0, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>18, 'author_id'=>3, 'publisher_id'=>3, 'narrator_id'=>3],
            ['title' => 'نوپای ناب','sku' => '20155','status'=>1,'free'=>0,'price'=>15000,'discount_percent'=>0,'user_id' => 1,'category_id'=>12,'author_id'=>4,'publisher_id'=>2,'narrator_id'=>3],
            [ 'title' =>'قطار کار آفرینی','sku' => '23465','status'=> 1, 'free'=>0, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>12, 'author_id'=>1, 'publisher_id'=>3, 'narrator_id'=>1],
            [ 'title' =>'جنایت و مکافات','sku' => '75163','status'=> 1, 'free'=>1, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>14, 'author_id'=>3, 'publisher_id'=>4, 'narrator_id'=>2],
            [ 'title' =>'جوان خام ','sku' => '14960','status'=> 1, 'free'=>0, 'price'=>15000, 'discount_percent'=>0, 'user_id' => 1,'category_id'=>18, 'author_id'=>3, 'publisher_id'=>3, 'narrator_id'=>3],
        ]);
        DB::table('book_photo')->insert([
            ['photo_id'=>3,'book_id'=>1],
            ['photo_id'=>6,'book_id'=>2],
            ['photo_id'=>3,'book_id'=>3],
            ['photo_id'=>4,'book_id'=>4],
            ['photo_id'=>5,'book_id'=>5],
            ['photo_id'=>6,'book_id'=>6],
            ['photo_id'=>7,'book_id'=>7],
            ['photo_id'=>8,'book_id'=>8],
            ['photo_id'=>3,'book_id'=>9],
            ['photo_id'=>6,'book_id'=>10],
            ['photo_id'=>3,'book_id'=>11],
            ['photo_id'=>4,'book_id'=>12],
            ['photo_id'=>5,'book_id'=>13],
            ['photo_id'=>6,'book_id'=>14],
            ['photo_id'=>7,'book_id'=>15],
            ['photo_id'=>8,'book_id'=>16],
        ]);
        DB::table('book_voice')->insert([
            ['voice_id'=>1 ,'book_id'=>1],
            ['voice_id'=>2 ,'book_id'=>1],
            ['voice_id'=>7 ,'book_id'=>1],
            ['voice_id'=>8 ,'book_id'=>2],
            ['voice_id'=>3 ,'book_id'=>2],
            ['voice_id'=>4 ,'book_id'=>2],
            ['voice_id'=>7 ,'book_id'=>3],
            ['voice_id'=>5 ,'book_id'=>3],
            ['voice_id'=>6 ,'book_id'=>3],
            ['voice_id'=>7 ,'book_id'=>4],
            ['voice_id'=>8 ,'book_id'=>4],
            ['voice_id'=>9 ,'book_id'=>5],
            ['voice_id'=>10 ,'book_id'=>5],
            ['voice_id'=>11 ,'book_id'=>6],
            ['voice_id'=>12 ,'book_id'=>6],
            ['voice_id'=>13 ,'book_id'=>7],
            ['voice_id'=>14 ,'book_id'=>7],
            ['voice_id'=>15 ,'book_id'=>8],
            ['voice_id'=>16 ,'book_id'=>8],
            ['voice_id'=>1 ,'book_id'=>9],
            ['voice_id'=>2 ,'book_id'=>9],
            ['voice_id'=>7 ,'book_id'=>9],
            ['voice_id'=>8 ,'book_id'=>10],
            ['voice_id'=>3 ,'book_id'=>10],
            ['voice_id'=>4 ,'book_id'=>10],
            ['voice_id'=>7 ,'book_id'=>11],
            ['voice_id'=>5 ,'book_id'=>11],
            ['voice_id'=>6 ,'book_id'=>11],
            ['voice_id'=>7 ,'book_id'=>12],
            ['voice_id'=>8 ,'book_id'=>12],
            ['voice_id'=>9 ,'book_id'=>13],
            ['voice_id'=>10 ,'book_id'=>13],
            ['voice_id'=>11 ,'book_id'=>14],
            ['voice_id'=>12 ,'book_id'=>14],
            ['voice_id'=>13 ,'book_id'=>15],
            ['voice_id'=>14 ,'book_id'=>15],
            ['voice_id'=>15 ,'book_id'=>16],
            ['voice_id'=>16 ,'book_id'=>16],
        ]);
    }
}
