@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>کتاب ها</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
                <li class="active">داشبرد</li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right">لیست کتاب ها</h3>
                    <div class="text-left">
                        <a class="btn btn-app" href="{{route('books.create')}}">
                            <i class="fa fa-plus"></i> جدید
                        </a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @include('admin.partials.form-errors')
                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            <div>{{session('error')}}</div>
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            <div>{{session('success')}}</div>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th class="text-center">ردیف</th>
                                <th class="text-center">شناسه</th>
                                <th class="text-center">کد کتاب</th>
                                <th class="text-center">عنوان</th>
                                <th class="text-center">وضعیت انتشار</th>
                                <th class="text-center">وضعیت</th>
                                <th class="text-center">عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=1 @endphp
                            @foreach($books as $book)
                                <tr>
                                    <td class="text-right">{{$i}}</td>
                                    <td class="text-center">{{$book->id}}</td>
                                    <td class="text-center">{{$book->sku}}</td>
                                    <td>{{$book->title}}</td>
                                    @if($book->status==1)
                                        <td class="text-center"><span class="label label-success">منتشر شده</span></td>
                                    @else
                                        <td class="text-center"><span class="label label-danger">منتشر نشده</span></td>
                                    @endif
                                    @if($book->free==1)
                                        <td class="text-center"><span class="label label-success">رایگان</span></td>
                                    @else
                                        <td class="text-center"><span class="label label-danger">غیر رایگان</span></td>
                                    @endif
                                    <td class="text-center">
                                        <div class="display-inline-block">
                                            <form method="post" action="/admin/books/{{$book->id}}">
                                                @csrf
                                                <input type="hidden" name="_method" value="DELETE">
                                                <a href="{{route('books.edit',$book->id)}}"
                                                   class="btn btn-warning">ویرایش</a>
                                                <button type="submit" class="btn btn-danger ">حذف</button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                                @php $i++; @endphp
                            @endforeach
                            </tbody>
                        </table>

                        <div class="col-md-12" style="text-align: center">{{$books->links()}}</div>

                    </div>
                    <!-- /.table-responsive -->
                </div>
            </div>
        </section>
    </div>
@endsection