@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                داشبرد
                <small>تنظیم صفه اول</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title pull-right"> نمایش گروه ها در صفحه اول سایت</h3>
                </div>
            <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form method="post" action="/admin/settings/{{$setting->id}}">
                                @csrf
                                <input type="hidden" name="_method" value="PATCH">
                                <div class="form-group">
                                    <label>نمایش جدید ترین کتاب ها</label>
                                    <div>
                                        <input type="radio" name="last" value="1" @if($setting->last == 1) checked @endif> <span class="margin-l-10">نشان داده شود</span>
                                        <input type="radio" name="last" value="0" @if($setting->last == 0) checked @endif> <span>نشان داده نشود</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>نمایش پربازدیدترین کتاب ها</label>
                                    <div>
                                        <input type="radio" name="visit" value="1" @if($setting->visit == 1) checked @endif> <span class="margin-l-10">نشان داده شود</span>
                                        <input type="radio" name="visit" value="0" @if($setting->visit == 0) checked @endif> <span>نشان داده نشود</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>نمایش پرفروش ترین کتاب ها</label>
                                    <div>
                                        <input type="radio" name="order" value="1" @if($setting->order == 1) checked @endif> <span class="margin-l-10">نشان داده شود</span>
                                        <input type="radio" name="order" value="0" @if($setting->order == 0) checked  @endif> <span>نشان داده نشود</span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection